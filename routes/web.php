<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index');
Route::get('/portal', function(){
 return view('admin/login');
});
Auth::routes();
Route::match(['get', 'post'], 'musicadmin','AdminController@login');
Route::get('musicadmin/forgot','AdminController@forgot');
//*************************************************************************************// 
Route::group(['middleware' => ['CheckRole','auth'],'prefix' => 'admin'], function () {
    Route::get('/dashboard', 'HomeController@index');
    Route::get('/editprofile', 'AdminController@editprofile');
//*************************************************************************************//
    Route::get('/user', 'MemberController@admin_staffs');
    Route::match(['get', 'post'], 'adduser','MemberController@admin_addMember');
    Route::match(['get', 'put'], 'edituser/{id}','MemberController@admin_editMember');
    Route::get('/removeMember/{id}', 'MemberController@remove');
    Route::get('/orders', 'MemberController@getAmazonOrders');
    Route::get('/customers', 'MemberController@fetchCustomers');
    Route::get('/orderItems', 'MemberController@getOrderItems');
    Route::get('/editcode', 'MemberController@editcode');
    Route::post('/editcode', 'MemberController@editcode');
    Route::match(['get','put'],'/staff/staffdetail/{id}', 'MemberController@staffdetail');
    Route::match(['get','put'],'/customer/detail/{id}', 'MemberController@customerdetail');
    Route::get('/customer/contact', 'MemberController@contactdetail');
    Route::get('/customers_detail', 'MemberController@all_customer_detail');

    Route::get('admin/customer/detail',array('as'=>'admin/customer/detail','uses'=>'ItemController@download'));
    
     /*******new***/
//aircraft
   Route::match(['get','post'],'/addPlane', 'AirplanController@addPlans');
   Route::get('/airPlanList', 'AirplanController@airJetlist');
   Route::match(['get', 'put'], 'editAirplane/{id}','AirplanController@editPlan');
   Route::get('/removeAirCraft/{id}', 'AirplanController@removeAirCraft');
   Route::get('/viewAirCraft/{id}', 'AirplanController@viewAirCraft');

//CATEGORY
     Route::get('/category', 'AirplanController@category');
     Route::match(['get','post'],'/addCategory', 'AirplanController@addCategory');
     Route::match(['get', 'put'], 'editCategory/{id}','AirplanController@editCategory');
     Route::get('/removeCategory/{id}', 'AirplanController@removeCategory');

//facilities
     Route::get('/facility', 'AdminController@facility');
     Route::match(['get','post'],'/addfacility', 'AdminController@addfacility');
     Route::match(['get', 'put'], 'editfacility/{id}','AdminController@editfacility');
    Route::get('/removefacility/{id}', 'AdminController@removefacility');
//MRO Vendor
    Route::get('/vendor', 'AdminController@vendor');
     Route::match(['get','post'],'/addvendor', 'AdminController@addvendor');
     Route::match(['get', 'put'], 'editvendor/{id}','AdminController@editvendor');
    Route::get('/removeVendor/{id}', 'AdminController@removeVendor');

     

   /***new**/
//*************************************************************************************//    
    Route::match(['get','post'],'/pages/add', 'PagesController@add');
    Route::match(['get','put'],'/page/edit/{id}', 'PagesController@edit');
    Route::get('/page/remove/{id}', 'PagesController@remove');
    Route::get('/pages/list', 'PagesController@listpages');
    Route::post('/page/imageUpload', 'PagesController@imageUpload');
//*************************************************************************************//     
    Route::match(['get','post'],'/library/add', 'LibraryController@add');
    Route::get('/libraries', 'LibraryController@listEbook');
    Route::get('/downloadLibrary/{id}', 'LibraryController@downloadLibrary');
    Route::get('/removeLibrary/{id}', 'LibraryController@removeLibrary');
    Route::match(['get','put'],'/library/edit/{id}', 'LibraryController@edit');
//*************************************************************************************//
    Route::get('/getProducts', 'ProductsController@getProducts');
    Route::match(['get','post'],'/product/add', 'ProductsController@add');
    Route::get('/products', 'ProductsController@productlist');
    Route::match(['get','put'],'/product/edit/{id}', 'ProductsController@edit');
    Route::get('/removeProduct/{id}', 'ProductsController@removeProduct');
    Route::get('/products', 'ProductsController@productlist');
    Route::match(['get','post'],'/product/list', 'ProductsController@addcode');

    Route::match(['get','put'],'/product/insertcode/{id}', 'ProductsController@fetchinsert_codes');
    Route::match(['get','put'],'/product/promocode/{id}', 'ProductsController@fetchpromo_code');
    Route::post('/products', 'ProductsController@big_discount_code');
    //Route::get('/insertcode', 'ProductsController@fetchinsert_codes');
  

//*************************************************************************************//
    Route::match(['get','post'],'/question/add', 'QuestionsController@add');
    Route::get('/questions', 'QuestionsController@listQuestions');
    Route::match(['get','put'],'/question/edit/{id}', 'QuestionsController@edit');
    Route::get('/removeQuestion/{id}', 'QuestionsController@removeQuestion');
//*************************************************************************************//     
    Route::match(['get','post'],'/addTiles', 'PagesController@addTiles');
    Route::get('/tiles', 'PagesController@tiles');
    Route::match(['get','put'],'/editTile/{id}', 'PagesController@editTile');
    Route::get('/tile/remove/{id}', 'PagesController@removeTile');
    Route::get('/insertCodes', 'MemberController@insertCodes');
//*************************************************************************************//
    Route::match(['get','post'],'/video/add', 'VideoController@add');
    Route::get('/videos', 'VideoController@videoList');
    Route::match(['get','put'],'/video/edit/{id}', 'VideoController@edit');
    Route::get('/removeVideo/{id}', 'VideoController@removeVideo');

});
//*************************************************************************************// 
Route::get('/page/{slug}', 'PagesController@page');
Route::match(['get','post'],'/signup', 'MemberController@signup');
Route::match(['get','post'],'/postalcode', 'MemberController@signupcode');
Route::get('/frontpage', 'FrontController@FrontEnd');
Route::get('/frontpage2','FrontController@front_end');
Route::get('/frontpage', 'FrontController@product_list');
Route::get('/frontpages','FrontController@order_list');
Route::post('/frontpage', 'FrontController@add'); 

Route::match(['get','post'],'/detail', 'MemberController@mailchimp');
 


//*************************************************************************************// 