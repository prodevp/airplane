   @extends('layouts.adminapp')
   @section('content')

            <div class="page-inner">
               
                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="profile-image-container">
                                <img src="images/profile-picture.png" alt="">
                            </div>
                            <h3 class="text-center">Amily Lee</h3>
                            <p class="text-center">UI/UX Designer</p>
                            <hr>
                            <ul class="list-unstyled text-center">
                                <li><p><i class="fa fa-map-marker m-r-xs"></i>Melbourne, Australia</p></li>
                                <li><p><i class="fa fa-envelope m-r-xs"></i><a href="#">example@mail.com</a></p></li>
                                <li><p><i class="fa fa-link m-r-xs"></i><a href="#">www.themeforest.net</a></p></li>
                            </ul>
                            <hr>
                            <button class="btn btn-primary btn-block"><i class="fa fa-plus m-r-xs"></i>Follow</button>
                        </div>
                        <div class="col-md-9 m-t-lg">
                          <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Staff Profile</h4>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="input-Default" class="col-sm-2 control-label">Contact Email <span class="icon-envelope-open"></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input-Default">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input-help-block" class="col-sm-2 control-label">Mobile Phone <span class="icon-call-out"></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input-help-block">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input-placeholder" class="col-sm-2 control-label">Home Phone <span class="icon-call-out"></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input-placeholder" placeholder="placeholder">
                                            </div>
                                        </div> 
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="page-footer">
                    <div class="container">
                        <p class="no-s">2015 &copy; Modern by Steelcoders.</p>
                    </div>
                </div>
            </div><!-- Page Inner -->
   @endsection7