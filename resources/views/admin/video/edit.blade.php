@extends('layouts.adminapp')
@section('title', 'Edit Video')
@section('content')
<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Edit Video</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Edit Video</h4>
					</div>
					<div class="panel-body">
						{!! Form::open(array('method' => 'PUT' , 'enctype' => 'multipart/form-data')) !!}
						{{ csrf_field() }}
						<div class="form-group">
							<label  class="col-sm-2 control-label">Name</label>
							 <div class="col-sm-10 drop">
								<input class="form-control" name="name" type="text" value="{{$video->name}}">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Video</label>
							<div class="col-sm-10 drop">
							  <input type="file" name="video" value="{{old('video')}}">
							  	</div>
							   @if ($errors->has('video'))
							    <span class="help-block">
								<strong>{{ $errors->first('video') }}</strong>
							  </span>
							  @endif
						    <p class="text-center">-OR-</p>
						</div>
						<div class="form-group">
							<label for="input-success" class="col-sm-2 control-label">Description</label>
							<div class="col-sm-10 drop">
								<textarea class="summernote" name="description">{{$video->description}}</textarea>
							</div>
						</div>
						<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="form-control" name="insert_code" type="text" value="{{$video->insert_code}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
						<button type="submit" class="btn btn-primary">Save</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

</div>
@endsection
@section('script')
<script>
	$('.summernote').summernote({
		height: 350
	});
</script>
@endsection