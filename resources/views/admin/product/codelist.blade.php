@extends('layouts.adminapp')
@section('title', 'Insertcodes')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Insert Codes</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Insert Codes</h4>
      </div>
      <div class="panel-body">
       
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>ID</th>
              <th>Insert Code</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($products_id as $insert_code)
           <tr>
             <td>{{$insert_code->id}}</td>
            <td> {{$insert_code->insert_code}}</td>
           <td> 
             @if($insert_code->status == "1") 
             <option value="">Unused</option>
              @elseif($insert_code->status =="0")
              <option value="">Used</option>    
            @endif 
             </td>
          </tr>
          @endforeach
        </tbody>
      </table>  
    </div> 
  </div>
</div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection
 