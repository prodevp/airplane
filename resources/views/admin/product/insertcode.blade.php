@extends('layouts.adminapp')
@section('title', 'Insert_codes')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Insert Codes</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Insert Codes</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
             
              <th>Insert Code</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($insert_codes as $insertcode)
           <tr>
             <td>{{$insertcode->product_id}}</td>        
            <td> {{$insertcode->insert_code}}</td>
            <td> {{$insertcode->status}}</td> 
          </tr>
          @endforeach
        </tbody>
      </table>  
    </div>
  </div>
</div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection

 