@extends('layouts.adminapp')  
@section('title', 'Add Product')
@section('content')
<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add Product</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Product</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Product Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Product Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Image</label>
								<div class="col-sm-10 drop">
									<input type="file" name="image" value="{{old('image')}}">
								</div>
								@if ($errors->has('image'))
								<span class="help-block">
									<strong>{{ $errors->first('image') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group">
							<label for="input-success" class="col-sm-2 control-label">Description</label>
								<div class="col-sm-10">
									<textarea class="summernote" name="description">{{old('description')}}</textarea>
								</div>
							</div>
							 <div class="form-group">
							    	<label  class="col-sm-2 control-label">SKU</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="SKU" name="sku" type="text" value="{{old('sku')}}">
							    		@if ($errors->has('sku'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('sku') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Insert Code</label>
									<div class="col-sm-10 drop">
										<input type="file" name="ecode" value="{{old('ecode')}}">
									@if ($errors->has('ecode'))
									<span class="help-block">
										<strong>{{ $errors->first('ecode') }}</strong>
									</span>
									@endif
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Promo code</label>
									<div class="col-sm-10 drop">
										<input type="file" name="pcode" value="{{old('pcode')}}">
									</div>
									@if ($errors->has('pcode'))
									<span class="help-block">
										<strong>{{ $errors->first('pcode') }}</strong>
									</span>
									@endif
								</div>
							   <!-- <div class="form-group">
							   	<label class="col-sm-2 control-label">Big Discount Code</label>
							   	<div class="col-sm-10 drop">
							   		<input type="file" name="big_code" value="{{old('big_code')}}">
							   	</div>
							   	@if ($errors->has('big_code'))
							   	<span class="help-block">
							   		<strong>{{ $errors->first('big_code') }}</strong>
							   	</span>
							   	@endif
							   </div> -->
							<button type="submit" class="btn btn-primary">Save</button>
						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>
@endsection
@section('script')
<script>
	$('.summernote').summernote({
		height: 350
	});
</script>
@endsection
