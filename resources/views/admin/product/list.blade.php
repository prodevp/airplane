@extends('layouts.adminapp')
@section('title', 'Products')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Products</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
      <div class="pull-right"><input type="file" name="big_code" value="{{old('big_code')}}"></div>
        <div class="pull-right"><a class="btn btn-info" href=" {{ url('/admin/big_discount_code') }}">Add</a></div>
      
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Name</th>
              <th>Image</th>
              <th>Promo Code</th>
              <th>SKU</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          @php
          //echo "<pre>"; print_r($products); die();
          @endphp
          @foreach ($products as $product)
           <tr>
           <td><a target="_blank" href="{{url('admin/product/insertcode/'.$product->id)}}">{{ $product->name }}</a></td>
            <td>
             @if (file_exists(public_path('images/products/normal/'.$product->image)))
                  <img class="img-circle avatar" src="{{ asset('images/products/normal/'.$product->image) }}" width="50" height="50" alt="">
              @else
                  <img class="img-circle avatar" src="{{ $product->image }}" width="50" height="50" alt="">
              @endif 
            </td>
            <td><a target="_blank" href="{{url('admin/product/promocode/'.$product->id)}}"><i class="glyphicon glyphicon-">Promo Codes</i></a></td>
            <td>{{ $product->sku }}</td>
            <td>
            @if($product->amazon == 0)
              <a href="{{url('/admin/product/edit/'.$product->id)}}"><i class="fa fa-edit"></i></a>
              <a class="deleteProduct" href="{{url('/admin/removeProduct/'.$product->id)}}"><i class="fa fa-remove"></i></a>
            @endif  
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>  
    </div>
  </div>
</div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteProduct').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this Product.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection