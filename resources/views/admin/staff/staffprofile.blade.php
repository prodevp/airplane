@extends('layouts.adminapp')

@section('content')
<link href="/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css"/>
<div class="page-inner">
        
<form class="form-horizontal" method="POST" action = "/admin/staff/{{$staffsprofile->id}}" enctype = 'multipart/form-data'>
 {{ csrf_field() }}
                <div id="main-wrapper" class="container">
                @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                    <div class="row">

                        <div class="col-md-3">
                            <div class="profile-image-container">
                                <img src="/staffimages/thumbnail-300/{{ $staffsprofile->profile_pic }}" alt="" id="staffProfileImage">
                                <input type="file" name="profile_image">
                            </div>
                            <h3 class="text-center">{{$staffsprofile->name}}</h3>
                            <p class="text-center">{{$staffsprofile->role}}</p>
                            <hr>
                            <ul class="list-unstyled text-center">
                                <li><p><i class="fa fa-envelope m-r-xs"></i><a href="mailto:{{$staffsprofile->email}}">{{$staffsprofile->email}}</a></p></li>
                            </ul>
                            <hr>
                            <div class="staProfile panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Staff login</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                      {!! Form::text('User[username]',$staffsprofile->username,['class'=>'form-control','placeholder' => 'Username']) !!}
                                      {!! Form::password('User[password]',['class'=>'form-control','placeholder' => 'Password']) !!}
                                      </div>
                                </div>
                            </div>
                            <div class="staProfile panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Permission Group</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                       {!! Form::select('User[role]', ['frontdesk' => 'Front Desk', 'manager' => 'Manager','newhire' => 'New Hire Training','webmaster' => 'Webmaster','staff' => 'Teacher'],$staffsprofile->role,['class'=>'form-control'] ) !!}
                                      </div>
                                </div>
                            </div>
                            <div class="staProfile panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Gender</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        {!! Form::select('User[gender]', ['m' => 'Male', 'f' => 'Female'],$staffsprofile->gender,['class'=>'form-control'] ) !!}
                                      </div>
                                </div>
                            </div>

                            <!-- <button class="btn btn-primary btn-block"><i class="fa fa-plus m-r-xs"></i>Follow</button> -->
                        </div>
                        <div class="col-md-6 m-t-lg">
                          <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">{{$staffsprofile->name}}</h4>
                                </div>
                                <div class="panel-body">                                                                        
                                        <div class="form-group">
                                            <label for="input-Default" class="col-sm-2 control-label">Contact</label>
                                            <div class="col-sm-8">
                                                <!-- <span class="icon-envelope-open"></span> -->
                                                <input type="text" class="form-control" id="email" name ="User[email]" placeholder="Email" value="{{$staffsprofile->email}}">

                                                <input type="text" class="form-control" id="Mobile" name ="User[phone]" placeholder="Mobile number" value="{{$staffsprofile->phone}}">

                                                <input type="text" class="form-control" id="Homephn" name ="StaffInfo[home_no]" placeholder="Home phone" value="{{$staffsprofile->staffdata->home_no}}">

                                                <input type="text" class="form-control" id="Workphn" name ="StaffInfo[work_no]" placeholder="Work phone" value="{{$staffsprofile->staffdata->work_no}}">

                                                <input type="text" class="form-control" id="Workext" name ="StaffInfo[work_ext]" placeholder="Work extension" value="{{$staffsprofile->staffdata->work_ext}}">

                                                <input type="text" class="form-control" id="Faxnumber" name ="StaffInfo[fax_no]" placeholder="Fax number" value="{{$staffsprofile->staffdata->fax_no}}">

                                                <input type="text" class="form-control" id="Othernum" name ="StaffInfo[other_no]" placeholder="Other number" value="{{$staffsprofile->staffdata->other_no}}">


                                                {!! Form::select('StaffInfo[mobile_provider]', ['48' => '48', 'ATT' => 'ATT','Bell Canada' => 'Bell Canada','Boost' => 'Boost'],$staffsprofile->staffdata->role,['class'=>'form-control'] ) !!}

                                                {!! Form::select('StaffInfo[type]', ['No notifications' => 'No notifications', 'Mobile text only' => 'Mobile text only','Email only' => 'Email only'],$staffsprofile->staffdata->role,['class'=>'form-control'] ) !!}

                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="input-Default" class="col-sm-2 control-label" >Address</label>
                                            <div class="col-sm-8">
                                               
                                                <input type="text" class="form-control" id="Addess" name ="StaffInfo[address]" placeholder="Address" value="{{$staffsprofile->staffdata->address}}">

                                                 {!! Form::select('StaffInfo[state]', ['North Carolina' => 'North Carolina', 'New York' => 'New York'],$staffsprofile->staffdata->role,['class'=>'form-control'] ) !!}

                                                 {!! Form::select('StaffInfo[country]', ['UNITED STATE' => 'UNITED STATE', 'INDIA' => 'INDIA'],$staffsprofile->staffdata->role,['class'=>'form-control'] ) !!}
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Private note</label>
                                            <div class="col-sm-10">
                                                <textarea class="summernote" name = "StaffInfo[private_note]" id = "privateSummernote"> {{$staffsprofile->staffdata->private_note}} </textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Public Biography</label>
                                            <div class="col-sm-10">
                                                <textarea class="summernote" name = "StaffInfo[public_bio]" id = "publicBio">{{$staffsprofile->staffdata->public_bio}}</textarea>
                                            </div>
                                        </div                                             
                                        </div>
                                       <button type="submit" class="btn btn-primary">Save</button>                               
                                </div>
                            </div>
                        </div>
                            @php
                            $settings = json_decode($staffsprofile->staffdata->settings);
                            @endphp
                        <div class="col-md-3">
                             <div class="staProfile panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Settings</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[desk_staff]','',isset($settings->desk_staff)); !!}
                                                            </span>
                                                            </div>
                                                            Desk staff
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[staff_appointments]','',isset($settings->staff_appointments)); !!}
                                                            </span>
                                                            </div>
                                                            Staff (for appointments)
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                             {!! Form::checkbox('StaffSetting[staff_classes]','',isset($settings->staff_classes)); !!}
                                                            </span>
                                                            </div>
                                                            Staff (for classes)
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[independent_contractor]','',isset($settings->independent_contractor)); !!}
                                                            </span>
                                                            </div>
                                                            Independent contractors
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[allow_overlapping]','',isset($settings->allow_overlapping)); !!}
                                                            </span>
                                                            </div>
                                                            Allow overlapping scheduling
                                                        </label>
                                     </div>
                                       <div class="input-group">                                          
                                           <input class="form-control" name="StaffInfo[schedule_order]" type="number" id="schedule_order" value="0">
                                             <label>Schedule sort order</label>
                                        </div><!-- /input-group -->
                                </div>
                            </div>


                             <div class="staProfile panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Sales settings</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                    <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[rep]','',isset($settings->rep)); !!}
                                                            </span>
                                                            </div>
                                                            Rep 1
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[assigned_followups]','',isset($settings->assigned_followups)); !!}
                                                            </span>
                                                            </div>
                                                            Can be assigned followups
                                                        </label>
                                     </div>
                                     <div class="checkbox">
                                                        <label>
                                                            <div class="checker">
                                                            <span> 
                                                            {!! Form::checkbox('StaffSetting[earn_tips]','',isset($settings->earn_tips)); !!}
                                                            </span>
                                                            </div>
                                                            Earns tips
                                                        </label>
                                     </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                </div>
               
</form>
                <div class="page-footer">
                    <div class="container">
                        <p class="no-s">2015 &copy; Modern by Steelcoders.</p>
                    </div>
                </div>
            </div>
  <script>
         $(document).ready(function() {   
        $('.summernote').summernote({
      height: 150
  });
         
       });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
<!-- <script src="/js/upload.js"></script> -->
<!-- <style type="text/css">
    .dropzone {
    border: 2px dashed #EB7260;
    box-shadow: 0 0 0 5px #373b44;
    background: #373b44;
    color: #fff;
    text-align: center;
}

.dropzone h3 {
    color: white;
    text-align: center;
    line-height: 3em;
    margin-top: 20px;
}

.dz-clickable {
    cursor: pointer;
}

.dz-drag-hover {
    border: 2px solid #EB7260;
}

.dz-preview, .dz-processing, .dz-image-preview, .dz-success, .dz-complete {
    background-color: rgba(0, 0, 0, .5);
    padding: 5px;
}

.dz-preview {
    width: auto !important;
}

.dz-image {
    text-align: center;
    /*border: 1px solid #EB7260;*/
}

.dz-details {
    padding: 10px;
}

.dz-success-mark, .dz-error-mark,  {
    display: none;
}
#book-image.dropzone.dz-clickable {
    margin-top: -80px;
}
#book-image .dz-default.dz-message {
    margin: 0 0 15px 0;
}
#book-image > div > h3 {
    line-height: 1;
    margin-top: 70px;
}
.staProfile .panel-heading {
    height: auto;
    padding: 10px 20px;
}
.staProfile .panel-body {
    padding: 0 !important;
}
.staProfile .panel-body .form-group{
    margin-bottom: 0;
}
</style> -->
 @endsection 
