@extends('layouts.adminapp')
@section('content')
 <div class="page-inner" style="min-height:51px !important">
   <div class="page-title">
                    <div class="container">
                        <h3>Staff Appointment Setup</h3>
                    </div>
    </div>
   <div id="main-wrapper" class="container">
	  <div class="row">
          <div class="col-md-12">
          	<div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Set default pay rate for {{$userdata->name}}</h3>
                                </div>
                                <div class="panel-body">
                                <span style="display: none;" class="sta_id">{{$userdata->id}}</span>
                                <span style="display: none;" class="sta_type">{{$userdata->staffdata->pay_type}}</span>
                                <div class="stafftype_select" style="display: none;">
                                {!! Form::select('StaffInfo[pay_type]', ['No pay' => 'No pay', 'Flat Rate' => 'Flat Rate','Percentage Rate' => 'Percentage Rate'],$userdata->staffdata->pay_type,['class'=>'form-control rateValue','onchange' => 'changeInput()']) !!}
                                <div class="staff_input">
                                @if ($userdata->staffdata->pay_type == 'Flat Rate')
							    <div class="input-group m-b-sm typeInput">
							    	<span class="input-group-addon" id="basic-addon1">$</span>
							    	<input class="form-control type_val" value="{{$userdata->staffdata->pay_value}}" placeholder="Enter Price" aria-describedby="basic-addon1" type="text">
							    </div>
								@elseif ($userdata->staffdata->pay_type == 'Percentage Rate')
							    <div class="input-group m-b-sm typeInput">
							    	<input class="form-control type_val" value="{{$userdata->staffdata->pay_value}}" placeholder="Enter Percentage" aria-describedby="basic-addon1" type="text">
							    	<span class="input-group-addon" id="basic-addon1">%</span>
							    </div>
							    @endif
							    </div>
							    <a href="javascript:void(0)" onclick="savePayRate()" class="btn btn-success">Save</a>

                                </div>
                                <div class="staff_type">
                                	<div class="staff_value">
                                	@if ($userdata->staffdata->pay_type == 'Flat Rate')
									    <span class="staff_type_name">{{$userdata->staffdata->pay_type}} : ${{$userdata->staffdata->pay_value}}</span>
									@elseif ($userdata->staffdata->pay_type == 'Percentage Rate')
									    <span class="staff_type_name">{{$userdata->staffdata->pay_type}} : {{$userdata->staffdata->pay_value}}%</span>

									@elseif ($userdata->staffdata->pay_type == 'No pay')
									    <span class="staff_type_name">No pay</span>
								    @endif
                                		
                                	</div>
                                <a class="edit_rate" attr-id="{{$userdata->id}}" href="javascript:void(0)">Edit</a>
                                 </div>
                                </div>
            </div>
            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Set up appointments for {{$userdata->name}}</h3>
                                </div>
                                <div class="panel-body">

                                </div>
            </div>
          </div>
      </div>
   </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="/js/staff/appointment.js"></script>
@endsection