@extends('layouts.adminapp')
@section('title','Users')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Users</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">

        <h4 class="panel-title">Users</h4>

          <div class="pull-right"><a class="btn btn-info" target="blank" href=" {{ url('admin/adduser') }}">Add</a></div>

       
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($staffs as $staff)
            <tr>
              <td><a target="_blank" href="{{url('admin/staff/staffdetail/'.$staff->id)}}">{{ $staff->name }}</a></td>
              <td>{{ $staff->email }}</td>
              <td>{{ $staff->phone}} </td>
              <td><a href="{{url('/admin/edituser/'.$staff->id)}}"><i class="fa fa-edit"></i></a><a class="deletePage" href="{{url('/admin/removeMember/'.$staff->id)}}"><i class="fa fa-remove"></i></a></td>
            </tr>
            @endforeach
          </tbody>
        </table>  
      </div>
    </div>
  </div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection  
@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deletePage').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this User.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection

