@extends('layouts.adminapp')
@section('title', 'Add Users')
@section('content')
<div class="page-inner" style="min-height:51px !important">

    <div class="page-title">
        <div class="container">
            <h3>Add Users</h3>
        </div>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Add Users</h4>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control"  placeholder="Name" type="text" name="name" value="{{old('name')}}">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input class="form-control" placeholder="Email" type="email" name="email" value="{{old('email')}}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input class="form-control" placeholder="Password" type="password" name="password">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" placeholder="Phone" type="text" name="phone" value="{{old('phone')}}">
                                @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                            <a href=" {{ url('admin/user') }}"><button type="button" class="btn btn-primary">Back</button></a>

                        </form>
                    </div>
                </div>
            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->

</div>
@endsection                        