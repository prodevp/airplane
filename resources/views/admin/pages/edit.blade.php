@extends('layouts.adminapp')
@section('title', 'Edit Page')
@section('content')
<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Edit Page</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Edit Page</h4>
					</div>
					<div class="panel-body">
						{!! Form::model($page, ['method' => 'PUT' ]) !!}
						
						<div class="form-group">
							<label>Title</label>
							<input class="form-control title" placeholder="Title" type="text" name="title" value="{{$page->title}}">
							@if ($errors->has('title'))
							<span class="help-block">
								<strong>{{ $errors->first('title') }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group">
							<label>Slug</label>
							<input class="form-control slug" placeholder="slug" type="text" name="slug" value="{{$page->slug}}">
							@if ($errors->has('slug'))
							<span class="help-block">
								<strong>{{ $errors->first('slug') }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="summernote" name="content">{{$page->content}}</textarea>
							@if ($errors->has('content'))
							<span class="help-block">
								<strong>{{ $errors->first('content') }}</strong>
							</span>
							@endif
						</div>

						<button type="submit" class="btn btn-primary">Edit</button>
					</form>
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

</div>
@endsection  
@section('script')
<script>
	var siteurl = $("meta[name=siteurl]").attr("content"); 
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	}); 
	$('.summernote').summernote({
		height: 350,
		onImageUpload:function(files,editor,$editable){
			sendFile(files[0]);
		}
	});

	function sendFile(file,editor,editable){
		data = new FormData();
		data.append('file',file);
		$.ajax({
			url: siteurl+'admin/page/imageUpload',
			type: 'POST',
			data: data,
			cache:false,
			contentType:false,
			processData:false
		})
		.done(function(e) {
			var editor = $.summernote.eventHandler.getEditor();
			editor.insertImage($('.note-editable'),siteurl+'images/gallery/'+e);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
	$(".title").keyup(function() {
		var slug = convertToSlug($(this).val());
		$('.slug').val(slug);
	});

	function convertToSlug(Text)
	{
		return Text
		.toLowerCase()
		.replace(/ /g,'-')
		.replace(/[^\w-]+/g,'')
		;
	}

</script>
@endsection