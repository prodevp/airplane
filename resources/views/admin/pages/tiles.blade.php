@extends('layouts.adminapp')
@section('title', 'Tiles')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Tiles</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Tiles</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Image</th>
              <th>Insert Code</th>
              <th>SKU</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tiles as $tile)
            <tr>
              <td><a target="_blank" href="{{$tile->link}}">{{ ucfirst($tile->name) }}</a></td>
              <td>{{ $tile->type }}</td>
              <td><?php if($tile->type == 'image'){ ?>
                <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}">
                <?php  }else{ ?>{{ $tile->image }}
                <?php } ?>
              </td>
              <td><?php if($tile->all_codes){ ?>
                All Codes
                <?php }else{ ?>
                {{ $tile->insert_code }}
                <?php } ?></td>
                <td>{{$tile->sku}}</td>
                <td><a href="{{url('/admin/editTile/'.$tile->id)}}"><i class="fa fa-edit"></i></a><a class="deleteTile" href="{{url('/admin/tile/remove/'.$tile->id)}}"><i class="fa fa-remove"></i></a></td>
              </tr>
              @endforeach

            </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div><!-- Row -->
</div>
</div>
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteTile').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this tile.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection