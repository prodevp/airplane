@extends('layouts.adminapp')
@section('title', 'Pages')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Pages</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Pages</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Title</th>
              <th>Slug</th>
              <th>Created at</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($pages as $page)

            <tr>
              <td><a target="_blank" href="{{url('/page/'.$page->slug)}}">{{ $page->title }}</a></td>
              <td>{{ $page->slug }}</td>
              <td>{{ $page->created_at }}</td>
              <td><a href="{{url('/admin/page/edit/'.$page->id)}}"><i class="fa fa-edit"></i></a><a class="deletePage" href="{{url('/admin/page/remove/'.$page->id)}}"><i class="fa fa-remove"></i></a></td>
            </tr>
            @endforeach
          </tbody>
        </table>  
      </div>
    </div>
  </div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deletePage').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this page.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection
