@extends('layouts.adminapp')
@section('title', 'Add Tiles')
@section('content')
<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add Tiles</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Tiles</h4>
					</div>
					<div class="panel-body">
						<input type="radio" class="radio" value="image" name="codeType">Image
						<input type="radio" class="radio" value="text" name="codeType">Text
						<form style="display: none;" method="post" class="form-horizontal image tilesForm" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Position</label>
								 <div class="col-sm-10">
                               <select name="position" class="form-control">
                                       <option value="">Select Position</option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option> 
                                   </select>

                               @if ($errors->has('category'))<p style="color:red;">{!!$errors->first('category')!!}</p>@endif
                                  </div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
									<input type="hidden" name="type" value="image">
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Image</label>
								<div class="col-sm-10">
									<input name="image" type="file">
									@if ($errors->has('file'))
									<span class="help-block">
										<strong>{{ $errors->first('file') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Link</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Link" name="link" type="text" value="{{old('link')}}">
									@if ($errors->has('link'))
									<span class="help-block">
										<strong>{{ $errors->first('link') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="allCoupon" type="checkbox"/>All Coupon
									<input class="form-control insert_code" placeholder="Enter Insert Code" name="insert_code" type="text" value="{{old('insert_code')}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
                             <div class="form-group">
                                	<label  class="col-sm-2 control-label">SKU</label>
                                	<div class="col-sm-10">
                                		<input class="form-control" placeholder="SKU" name="sku" type="text" value="{{old('sku')}}">
                                		@if ($errors->has('sku'))
                                		<span class="help-block">
                                			<strong>{{ $errors->first('sku') }}</strong>
                                		</span>
                                		@endif
                                	</div>
                                </div>
							<button type="submit" class="btn btn-primary" >Save</button>
						</form>
						<form style="display: none;" method="post" class="form-horizontal text tilesForm" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
									<input type="hidden" name="type" value="text">
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Text</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Name" name="image" type="text" value="{{old('image')}}">
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Link</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Link" name="link" type="text" value="{{old('link')}}">
									@if ($errors->has('link'))
									<span class="help-block">
										<strong>{{ $errors->first('link') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="allCoupon" type="checkbox"/>All Coupon
									<input class="form-control insert_code" placeholder="Enter Insert Code" name="insert_code" type="text" value="{{old('insert_code')}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
							   	<label  class="col-sm-2 control-label">SKU</label>
							   	<div class="col-sm-10">
							   		<input class="form-control" placeholder="SKU" name="sku" type="text" value="{{old('sku')}}">
							   		@if ($errors->has('sku'))
							   		<span class="help-block">
							   			<strong>{{ $errors->first('sku') }}</strong>
							   		</span>
							   		@endif
							   	</div>
							   </div>
							<button type="submit" class="btn btn-primary" >Save</button>
						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>
@endsection  

@section('script')
<script>
	$(".radio").click(function() {    
		$('.tilesForm').slideUp('slow'); 
		$('.'+$(this).val()).slideDown('slow');
	});
	$('.allCoupon').change(function(){
		var val = $(this).prop('checked');
		$('.insert_code').attr('disabled',val);

	});
</script>
@endsection