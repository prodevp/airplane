@extends('layouts.adminapp')
@section('title', 'Edit Vendor')
@section('content')


<!-- country-state-->
<script src="{{ url('/js/Countries.js')}}" type="text/javascript"></script>
<!-- end of code -->

<!-- form validation -->
<script src="{{url('/js/form-validate.js')}}" type="text/javascript"></script>
<!--end of code -->

<!-- telephone -->
<link rel="stylesheet" href="{{ url('/css/intlTelInput.css')}}" />
<script src="{{ url('/js/intlTelInput.js')}}"></script>
<script src="{{ url('/js/utils.js')}}"></script>

<script src="{{ url('/js/frontend.js')}}"></script> 
<!-- telephone -->


<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Edit Vendor</h3>
		</div>
	</div>

	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Edit Vendor</h4>
					</div>
					<div class="panel-body">
						{!! Form::open(array('method' => 'PUT' , 'enctype' => 'multipart/form-data','class'=> 'vendor')) !!}
						{{ csrf_field() }}
						<div class="form-group clearfix">
								<label  class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Name" name="name" type="text" value="{{ $vendor->name}}">
									@if ($errors->has('name'))
									<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group clearfix">
								<label  class="col-sm-2 control-label">Company Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Company Name" name="company_name" type="text" value="{{$vendor->company_name}}">
									@if ($errors->has('Company_name'))
									<span class="help-block">
										<strong>{{ $errors->first('Company_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							 <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Address</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Address" name="address" type="text" value="{{$vendor->address}}">
							    		@if ($errors->has('address'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('address') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Country</label>
							    	<div class="col-sm-10">
							    	 <select id="country" name="country" class="form-control"></select> 

							    	 <input type="hidden" class="form-control cntry" value="{{ $vendor->country }}">  

							    	  <input type="hidden" class="form-control state" value="{{ $vendor->state }}">  
							    		
							    		@if ($errors->has('country'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('country') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">State</label>
							    	<div class="col-sm-10">
							    	 <select id="state" name="state" class="form-control"></select> 

							    	<!--   <input type="text" name="state" class="form-control" value="{{$vendor->state}}"> -->

							    		
							    		@if ($errors->has('state'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('state') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">City</label>
							    	<div class="col-sm-10">
							    	

							    	  <input type="text" name="city" class="form-control" value="{{$vendor->city}}">

							    		
							    		@if ($errors->has('city'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('city') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Facility</label>
							    	<div class="col-sm-10">
							    	 <!--  <select id="state" name="state" class="form-control"></select> -->

							    	  <input type="text" name="facility" class="form-control" value="{{$vendor->facility}}">
							    	  
							    		
							    		@if ($errors->has('facility'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('facility') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

                                <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Telephone
                                    </label>
							    	<div class="col-sm-10">
							    		<input class="form-control tel" placeholder="Telephone" name="telephone" type="tel" value="{{$vendor->telephone}}">
							    		<span id="error-msg" class="hide teErr">Invalid</span>
							    		<span id="valid-msg" class="hide">Valid</span>
							    		@if ($errors->has('telephone'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('telephone') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <input type="hidden" value="{{$vendor->telephone}}" class="teleph">

					       <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Email</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Email" name="email" type="email" value="{{$vendor->email}}">
							    		
							    		@if ($errors->has('email'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('email') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							 </div>

							    
							     <div class="form-group clearfix">
							    	<label  class="col-sm-2 control-label">Site Url</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Site Url" name="site" type="text" value="{{$vendor->site}}">
							    		@if ($errors->has('site'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('site') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>


							<button type="submit" class="btn btn-primary">Save</button>

							<a href=" {{ url('admin/vendor') }}"><button type="button" class="btn btn-primary">Cancel</button></a>


					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

</div>
<script>

$(document).ready( function (){
	var coutry = $('.cntry').val();

	var state = $('.state').val();
	
 setTimeout(function(){ 
 	populateCountries("country", "state" ,coutry,state);
}, 2000);
});
</script>
<style>
.intl-tel-input {
    width: 100%;
    margin-bottom: 2px;
}
</style>
@endsection
