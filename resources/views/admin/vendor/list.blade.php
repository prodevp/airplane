@extends('layouts.adminapp')
@section('title', 'Vendor')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Vendor</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
  
      
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">

          <thead>
            <tr>
            <th>Name</th>
              <th>Company Name</th>
              <th>Email</th>
               <th>Address</th>
                <th>Country</th>
                 <th>State</th>
                  <th>City</th>
                 <th>Telephone</th>
                 <th>Site Url</th>
                 <th>Facility</th>
                 <th>Created_at</th>
                 <th>Action</th>
              </tr>
          </thead>
          <tbody>
         
          @foreach ($vendor as $detail)
           <tr>
            <td>{{ $detail->name }}</td>
           <td>{{ $detail->company_name }}</td>
            <td>{{ $detail->email }}</td>
            <td>{{ $detail->address }}</td>
             <td>{{ $detail->country }}</td>
             <td>{{ $detail->state }}</td>
               <td>{{ $detail->city }}</td>
             <td>{{ $detail->telephone }}</td>
              <td>{{ $detail->site }}</td>
               <td>{{ $detail->facility }}</td>
                <td>{{ date('m/d/Y', strtotime($detail->created_at))}}</td>
                      <td><a href=" {{ url('admin/editvendor/'.$detail->id) }}"><i class="fa fa-edit"></i></a>/<a  class="deleteProduct" href=" {{ url('admin/removeVendor/'.$detail->id) }}"><i class="fa fa-remove"></i></a></td>

          </tr>
          @endforeach
        </tbody>
      </table>  
    </div>
  </div>
</div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteProduct').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this Product.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection