@extends('layouts.adminapp')

@section('content')
<div class="page-inner">
<div class="content">

Aircraft Variant: {{$product->airVariant}}
Aircraft Type Passenger or CARGO : {{$product->airType}}
Date of Manufacture: {{$product->manufacture}}
MSN: {{$product->MSN}}
Variable: {{$product->variable}}
Utilisation per Date: {{$product->utilisation}}
Total Time: {{$product->timeTaken}} Hours
Total Cycle: {{$product->cycle}} Cycles
Engine 1 (Left Hand Side)
Engine Type: {{$product->engineType}}
Engine Serial Number ESN: {{$product->ESN}}
Engine Thrust Category: {{$product->category}}
TT: {{$product->TT}} Hours
TC: {{$product->TC}} Cycles
Last Shop visit
Facility: {{$product->facility}}


At Engine TT: {{$product->engin_TC}} Hours
At Engine TC: {{$product->engin_TT}} Cycles
Reason of removal: {{$product->reason}}
TSLSV: {{$product->TSLSV}} Hours (calculated 25,000 -20,000)
CSLSV: {{$product->CSLSV}} Cycles (Calculated 10,000 – 2,000 )
Bought From: {{$product->bought}} 
Price: $ {{$product->price}} 



</div>
  </div>

 @endsection 
