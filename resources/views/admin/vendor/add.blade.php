@extends('layouts.adminapp')  
@section('title', 'Add MRO Vendor')
@section('content')

<!-- country-state-->
<script src="../js/Countries.js" type="text/javascript"></script>
<!-- end of code -->

<!-- form validation -->
<script src="../js/form-validate.js" type="text/javascript"></script>
<!--end of code -->

<!-- telephone -->
<link rel="stylesheet" href="../css/intlTelInput.css" />
<script src="../js/intlTelInput.js"></script>
<script src="../js/utils.js"></script>

<script src="../js/frontend.js"></script> 
<!-- telephone -->


<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add MRO Vendor</h3>
		</div>
	</div>
	

	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add MRO Vendor</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone vendor" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<label  class="col-sm-2 control-label">Company Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Company Name" name="company_name" type="text" value="{{old('Company_name')}}">
									@if ($errors->has('Company_name'))
									<span class="help-block">
										<strong>{{ $errors->first('Company_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Address</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Address" name="address" type="text" value="{{old('address')}}">
							    		@if ($errors->has('address'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('address') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group">
							    	<label  class="col-sm-2 control-label">Country</label>
							    	<div class="col-sm-10">
							    	   <select id="country" name="country" class="form-control"></select> 
							    	 
							    	 @if ($errors->has('country'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('country') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">State</label>
							    	<div class="col-sm-10">
							    	  <select id="state" name="state" class="form-control"></select>

							    		@if ($errors->has('state'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('state') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">City</label>
							    	<div class="col-sm-10">
							 

							    	 <input type="text" name="city" class="form-control" id="city">

							    		
							    		@if ($errors->has('city'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('city') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">Facility</label>
							    	<div class="col-sm-10">
							    	
							    	  <input type="text" name="facility" class="form-control">
							    	  
							    		
							    		@if ($errors->has('facility'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('facility') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

                                <div class="form-group">
							    	<label  class="col-sm-2 control-label">Telephone
                                    </label>
							    	<div class="col-sm-10">
							    	

							    		<input class="form-control tel" placeholder="Telephone" name="telephone" type="tel" value="{{old('site')}}">

							    		<input type="hidden" class="telHide" value="{{old('telephone')}}">
							    		<span id="error-msg" class="hide teErr">InValid</span>
							    		<span id="valid-msg" class="hide">Valid</span>


							    		@if ($errors->has('telephone'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('telephone') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

					       <div class="form-group">
							    	<label  class="col-sm-2 control-label">Email</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Email" name="email" type="email" value="{{old('email')}}">
							    		
							    		@if ($errors->has('email'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('email') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							 </div>

							    
							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Site Url</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Site Url" name="site" type="text" value="{{old('site')}}">
							    		@if ($errors->has('site'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('site') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     
							<button type="submit" class="btn btn-primary">Save</button>

							<a href=" {{ url('admin/vendor') }}"><button type="button" class="btn btn-primary">Back</button></a>


						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>

<style>
.intl-tel-input {
    width: 100%;
    margin-bottom: 2px;
}
</style>


@endsection

