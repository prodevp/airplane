@extends('layouts.adminapp')  
@section('title', 'Add AirPlane')
@section('content')
<style>
 .select-editable {
     position:relative;
     background-color:white;
     border:solid grey 1px;

 }
 .select-editable select {
     position:absolute;
     top:0px;
     left:0px;
     font-size:14px;
     border:none;
     margin:0;
 }
 .select-editable input {
     position:absolute;
     top:0px;
     left:0px;
     padding:1px;
     font-size:12px;
     border:none;
 }
 .select-editable select:focus, .select-editable input:focus {
     outline:none;
 }
 </style>
<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add Airplane</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Airplane</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Aircraft Variant</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Aircraft Variant" name="airVariant" type="text" value="{{old('airVariant')}}">
									@if ($errors->has('airVariant'))
									<span class="help-block">
										<strong>{{ $errors->first('airVariant') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Image</label>
								<div class="col-sm-10 drop">
									<input type="file" name="image" value="{{old('image')}}" id="imgInp">
									  <img id="blah" src="#" alt="Airplane image" style="width:200px;"/>
								</div>

							


								@if ($errors->has('image'))
								<span class="help-block">
									<strong>{{ $errors->first('image') }}</strong>
								</span>
								@endif
							</div>
							
							 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Aircraft Type</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Aircraft Type" name="airType" type="text" value="{{old('airType')}}">
							    		@if ($errors->has('airType'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('airType') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

                                 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Date of Manufacture
                                    </label>
							    	<div class="col-sm-10">
							    		<input class="form-control datepicker" placeholder="Date of Manufacture" name="manufacture" type="text" value="{{old('manufacture')}}">
							    		@if ($errors->has('manufacture'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('manufacture') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>


					     <div class="form-group">
							    	<label  class="col-sm-2 control-label">MSN</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="MSN" name="MSN" type="text" value="{{old('airType')}}">
							    		<span class="errRecord"></span>
							    		@if ($errors->has('MSN'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('MSN') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    
							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Utilisation per Date</label>
							    	<div class="col-sm-10">
							    		<input class="form-control datepicker" placeholder="Utilisation" name="utilisation" type="text" value="{{old('utilisation')}}">
							    		@if ($errors->has('utilisation'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('utilisation') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">Variable</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Variable" name="variable" type="text" value="{{old('variable')}}">
							    		@if ($errors->has('variable'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('variable') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>


                                 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Total Time</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Total Time" name="timeTaken" type="text" value="{{old('timeTaken')}}">
							    		<span class="errRecord"></span>
							    		@if ($errors->has('timeTaken'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('timeTaken') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">Total Cycle</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Total Cycle" name="cycle" type="text" value="{{old('cycle')}}">
							    		<span class="errRecord"></span>
							    		@if ($errors->has('cycle'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('cycle') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>
                                <div class="form-group">
							    	<label  class="col-sm-2 control-label">Engine Type</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Engine Type" name="engineType" type="text" value="{{old('engineType')}}">
							    		@if ($errors->has('engineType'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('engineType') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Engine Serial Number ESN</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="ESN" name="ESN" type="text" value="{{old('ESN')}}">
							    		@if ($errors->has('ESN'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('ESN') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Aircraft Operating at Thrust Category</label>
							    	<div class="col-sm-10">
							    	 <select name="category[]" multiple value="{{old('category')}}" class="selectpicker form-control">
									  <option value="">Select multiple Category</option>
									  @foreach ($category as $cat)

									  <option value="{{ $cat->name }}">{{ $cat->name }}</option>
									  @endforeach
									</select> 
										
							    		@if ($errors->has('category'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('category') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">TT</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="TT" name="TT" type="text" value="{{old('TT')}}">Hours
							    		<span class="errRecord"></span>
							    		@if ($errors->has('TT'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('TT') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">TC</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="TC" name="TC" type="text" value="{{old('TC')}}">Cycles

							    		<span class="errRecord"></span>

							    		@if ($errors->has('TC'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('TC') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group">
							    	<label  class="col-sm-2 control-label">Facility</label>
							    	<div class="col-sm-10">

							    	<div class="select-editable form-control">
                                    <select  value="{{old('facility')}}" class="form-control select-editable" onchange="this.nextElementSibling.value=this.value">
									  <option value=""></option>
									  @foreach ($facility as $cat)

									  <option value="{{ $cat->company_name }}">{{ $cat->company_name }}</option>
									  @endforeach
									</select> 
				 <input type="text" class="form-control" placeholder="Select Facility or type" name="facility" value="{{old('facility')}}"/>
</div>
                                        @if ($errors->has('facility'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('facility') }}</strong>
							    		</span>
							    		@endif
							    	
							    </div>
							    </div>

	                            <div class="form-group">
							    	<label  class="col-sm-2 control-label">At Engine TT</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Engine TT" name="engin_TT" type="text" value="{{old('engin_TT')}}">Hours

							    		<span class="errRecord"></span>

							    		@if ($errors->has('engin_TT'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('engin_TT') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">At Engine TC</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="At Engine TC" name="engin_TC" type="text" value="{{old('engin_TC')}}">Cycles

							    		<span class="errRecord"></span>

							    		@if ($errors->has('engin_TC'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('engin_TC') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Reason of removal</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Reason of removal" name="reason" type="text" value="{{old('reason')}}">
							    		@if ($errors->has('reason'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('reason') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group">
							    	<label  class="col-sm-2 control-label">TSLSV</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="TSLSV" name="TSLSV" type="text" value="{{old('TSLSV')}}">
							    		<span class="errRecord"></span>
							    		@if ($errors->has('reason'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('TSLSV') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group">
							    	<label  class="col-sm-2 control-label">CSLSV</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="CSLSV" name="CSLSV" type="text" value="{{old('CSLSV')}}">
							    		<span class="errRecord"></span>
							    		@if ($errors->has('reason'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('CSLSV') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

                                <div class="form-group">
							    	<label  class="col-sm-2 control-label">Bought From</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Bought From" name="bought" type="text" value="{{old('bought')}}"> (optional)
							    		@if ($errors->has('bought'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('bought') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Price</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Price" name="price" type="text" value="{{old('price')}}">$ (optional)

							    		<span class="errRecord"></span>


							    		@if ($errors->has('price'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('price') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href=" {{ url('admin/airPlanList') }}"><button type="button" class="btn btn-primary">Back</button></a>
                  
						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>
@endsection

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="{{ url('/js/jquery.js') }}" type="text/javascript"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
  </script>



<!--*******selectbox -->
  <link rel="stylesheet" href="../css/selectbox/bootstrap-select.css">
  <script src="../js/selectbox/bootstrap-select.js" type="text/javascript"></script>

<!--*******selectbox -->


  <script>        
            
         $(document).ready(function() {
         	$('#blah').hide();
if($('#my-awesome-dropzone').find('.help-block').attr('class')){
    	if(localStorage.getItem("imagePath")){
	$('#blah').show();
  $('#blah').attr('src', localStorage.getItem("imagePath"));	
}
}else{
localStorage.setItem("imagePath",'');
}
    
        $(".int").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {

                $(this).parents('.form-group').find('.errRecord').text('(Fill only integer value)').show();
                e.preventDefault();
            } else {
                $(this).parents('.form-group').find('.errRecord').text('(Fill only integer value)').hide();
            }
        });

        function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
    	$('#blah').show();
      $('#blah').attr('src', e.target.result);
      localStorage.setItem("imagePath",e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
    });
    </script>

