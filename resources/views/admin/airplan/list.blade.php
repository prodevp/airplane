@extends('layouts.adminapp')
@section('title', 'Airplans')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>AirPlane</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
    
        <div class="pull-right"><a class="btn btn-info" target="blank" href=" {{ url('admin/addPlane') }}">Add</a></div>
      
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
      


          <thead>
            <tr>
            <th>Action</th>
            <th>Component</th>
              <th>Aircraft Type</th>
              <th>Image</th>
              <th>Aircraft Variant</th>
              <th>Date of Manufacture</th>
              <th>MSN</th>
               <th>ESN</th>
                <th>Variable</th>
                 <th>Total Cycle</th>
                 <th>Utilisation per Date</th>
                 <th>Total Time</th>
                 <th>Engine Type</th>
                 <th>Engine Thrust Category</th>
                 <th>TT</th>
                 <th>TC</th>
                 <th>Price</th>
                 <th>Engine TT</th>
                <th>Engine TC</th>
                <th>TSLSV</th>
                <th>CSLSV</th>
                <th>Reason of removal</th>
                <th>Bought From</th>
                 <th>Facility</th>
                 <th>Created_at</th>
                 

            </tr>
          </thead>
          <tbody>
         
          @foreach ($products as $product)
           <tr>
           <td><a href=" {{ url('admin/editAirplane/'.$product->id) }}"><i class="fa fa-edit"></i></a>/<a  class="deleteProduct" href=" {{ url('admin/removeAirCraft/'.$product->id) }}"><i class="fa fa-remove"></i></a>/<a  href=" {{ url('admin/viewAirCraft/'.$product->id) }}">View</a></td>
  
          <td><a href=" {{ url('admin/component/'.$product->id) }}"><button>LPP Component</button></a></td>
           <td>{{ $product->airType }}</td>
            <td>
             @if (file_exists(public_path('images/airplan/'.$product->image)))
                  <img class="img-circle avatar" src="{{ asset('images/airplan/'.$product->image) }}" width="50" height="50" alt="">
              @else
                  <img class="img-circle avatar" src="{{ $product->image }}" width="50" height="50" alt="">
              @endif 
            </td>
            <td>{{ $product->airVariant }}</td>

            <td>{{ $product->manufacture }}</td>
             <td>{{ $product->MSN }}</td>
              <td>{{ $product->ESN }}</td>
               <td>{{ $product->variable }}</td>
                <td>{{ $product->cycle }}</td>
                 <td>{{ $product->utilisation }}</td>
                  <td>{{ $product->timeTaken }}</td>
                   <td>{{ $product->engineType }}</td>

                     @php  
       $get = json_decode($product->category);
       $response;
       $result = '';
        if(count($get) > 0){
      
        for($i=0;$i<count($get);$i++){ 
        $val = $i+1;
          if($val == count($get)){
          $comma = '';
          }else{
          $comma = ',';
          }
          if($result){
          $response =$result.$get[$i].$comma;
        $result = $result.$get[$i].$comma;
          }else{
         $response = $get[$i].$comma; 
         $result =  $get[$i].$comma; 
          }
        }
        } else{
        $response ='';
        }
      @endphp

                    <td>{{ $response }}</td>
                     <td>{{ $product->TT }} Hours</td>
                      <td>{{ $product->TC }} Cycles</td>
                       <td>{{ $product->price }}</td>
                      <td>{{ $product->engin_TT }} Hours</td>
                      <td>{{ $product->engin_TC }} Cycles</td>
                       <td>{{ $product->TSLSV }}</td>
                     <td>{{ $product->CSLSV }}</td>
                     <td>{{ $product->reason }}</td>
                    <td>{{ $product->bought }}</td>
                      <td>{{ $product->facility }}</td>
                      <td>{{ date('m/d/Y', strtotime($product->created_at))}}</td>
                      

          </tr>
          @endforeach
        </tbody>
      </table>  
    </div>
  </div>
</div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteProduct').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this Product.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection