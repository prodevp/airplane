@extends('layouts.app')


@section('content')

<h2 class="text-center">MailChimp API Example</h2>

<div class="container">


@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

  <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

</div>

@endif


@if ($message = Session::get('error'))

<div class="alert alert-danger alert-block">

  <button type="button" class="close" data-dismiss="alert">×</button> 

        <strong>{{ $message }}</strong>

</div>

@endif


  <div class="row">

    <div class="col-md-5">

      <div class="well">

                
          {{ Form::open(['url' => 'detail', 'method' => 'post'] ) }}
                                 {{ csrf_field() }}
           
              
                 <div>

                  <h3 class="text-center">Subscribe Your Email</h3>

                   <input class="form-control" name="email" id="email" type="email" placeholder="Your Email"  >

                   <br/>

                   <div class="text-center">

                    <button class="btn btn-info btn-lg" type="submit">Subscribe</button>

                   </div>

                </div>
      
                                {{ Form::close() }}
               

         </div>

    </div>


  </div>

</div>

@endsection