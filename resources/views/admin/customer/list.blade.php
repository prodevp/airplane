@extends('layouts.adminapp')
@section('title', 'Customers')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Customers</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">

      <!-- <form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="{{url('/admin/customer/list')}}" enctype="multipart/form-data">
             {{ csrf_field() }}
      <div class="pull-right">
       <button type="submit" class="btn btn-info">Import Insert Code</button>

       <input type="file" name="ebook" value="{{old('ebook')}}" class="pull-right">
           </form>
         </div> -->

        <div class="pull-right"><a class="btn btn-info" href="{{ url("/admin/orders") }}">Import Customers</a></div>
        <div class="pull-right"><a class="btn btn-info" href="{{ url('/admin/customers_detail') }}">Export Pdf</a></div>
        <h4 class="panel-title">Customers</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Order Number</th>
              <th>Name</th>             
              <th>Email</th>
              <th>Address</th>
              <th>City</th>
              <th>Zipcode</th>
              <th>Country</th>
             <!--  <th>Action</th> -->
            </tr>
          </thead>
          <tbody>
            @foreach ($customers as $customer)
            <tr>
              <td> {{ $customer->order_number }} </td>
              <td>{{ ucfirst($customer->name) }}</td>
              <td>{{ $customer->email }}</td>              
              <td>{{ $customer->address_line }}</td>
              <td>{{ $customer->city }}</td>
              <td>{{ $customer->postal_code }}</td>
              <td>{{ $customer->country_code }}</td>
              <!-- <td></td> -->
              <!-- <td > --><!-- Trigger the modal with a button -->
                 <!-- <span class="code_field" id="code-id{{ $customer->id }}" attr-id="{{ $customer->id }}"> @if (!empty ($customer->insert_code)) {{ $customer->insert_code }}</span>
                <a href="#" class="editcode"><i class="fa fa-edit"></i></a>
              @else
              <button type="button" class="btn btn-info btn-lg insertcode" id="add_code{{ $customer->id }}" data-toggle="modal" c_id ="{{ $customer->id }}">
              Add Code</button>
              @endif 
            </td> -->
            </tr>
            @endforeach
            </tbody>
           </table>  
          </div>
        </div>
      </div>
    </div>
    </div><!-- Row -->
    </div>
    </div><!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           {!! Form::model(  ['method' => 'POST','id'=>'promocode_form' ]) !!}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            <h4 class="modal-title">Add Insert Code</h4>
          </div>
          <div class="form-group">
          <label for="msg">Insert Code:</label>
          <input type="text" class="form-control insertcode_cus" id="msg" value="" name="insert_code" >
          <input type="hidden" name="id" value="" class="cid">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="add_promocode">ADD</button>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
@endsection
@section('script')
<script>  

$(".insertcode").click(function(){
   $('#myModal').modal('toggle');
   var cid = $(this).attr("c_id");
   var id = $('.cid').val(cid);
});
/****************************************************/
$(document).on("click", "#add_promocode", function(){

  var customer_id = $('.cid').val();
  var code = $(".insertcode_cus").val();

 formdata =  $('#promocode_form').serialize();
  $.ajax({
               type:'POST',
               url: '{{url("/admin/editcode")}}',
               data:formdata,
               success:function(data){

                   if(data == "success") {                     
                      $("#add_code"+customer_id).hide();
                      $("#code-id"+customer_id).text(code);
                      $(".insertcode_cus").val("");
                      $('#myModal').modal('hide');
                   }
 
               }
            });

     });     
/****************************************************/
$(".editcode").click(function(){
   $('#myModal').modal('toggle');
   var id = $(this).prev('.code_field').text();
   var cid = $(this).prev('.code_field').attr('attr-id');
   //alert(cid);
    var data = $('#msg').val(id);
    var id = $('.cid').val(cid);
});

</script>
@endsection