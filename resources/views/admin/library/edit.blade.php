@extends('layouts.adminapp')
@section('title', 'Edit Library')
@section('content')
<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Edit Library</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Edit Ebook</h4>
					</div>
					<div class="panel-body">
						{!! Form::model($library, ['method' => 'PUT' ]) !!}
						{{ csrf_field() }}
						<div class="form-group">
							<label  class="col-sm-2 control-label">Ebook Name</label>
							 <div class="col-sm-10 drop">
								<input class="form-control" name="name" type="text" value="{{$library->name}}">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">File</label>
							  <input type="file" name="ebook" value="{{old('ebook')}}">
							  <div class="col-sm-10 drop">
							   @if ($errors->has('ebook'))
							    <span class="help-block">
								<strong>{{ $errors->first('ebook') }}</strong>
							  </span>
							  @endif
							</div>
						</div>
						<div class="form-group">
							<label for="input-success" class="col-sm-2 control-label">Description</label>
							<div class="col-sm-10 drop">
								<textarea class="summernote" name="description">{{$library->description}}</textarea>
							</div>
						</div>
						<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="form-control" name="insert_code" type="text" value="{{$library->insert_code}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
						<button type="submit" class="btn btn-primary">Save</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

</div>
@endsection
@section('script')
<script>
	$('.summernote').summernote({
		height: 350
	});
</script>
@endsection
