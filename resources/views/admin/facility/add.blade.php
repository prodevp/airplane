@extends('layouts.adminapp')  
@section('title', 'Add Facility')
@section('content')

<!-- country-state-->
<script src="../js/Countries.js" type="text/javascript"></script>
<!-- end of code -->

<!-- form validation -->
<script src="../js/form-validate.js" type="text/javascript"></script>
<!--end of code -->

<!-- telephone -->
<link rel="stylesheet" href="../css/intlTelInput.css" />
<script src="../js/intlTelInput.js"></script>
<script src="../js/utils.js"></script>

<script src="../js/frontend.js"></script> 
<!-- telephone -->


<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add Facility</h3>
		</div>
	</div>
	

	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Facility</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone facility" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Company Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Company Name" name="company_name" type="text" value="{{old('company_name')}}">
									@if ($errors->has('company_name'))
									<span class="help-block">
										<strong>{{ $errors->first('company_name') }}</strong>
									</span>
									@endif
								</div>
							</div>

							 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Address</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Address" name="address" type="text" value="{{old('address')}}">
							    		@if ($errors->has('address'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('address') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>
                                
                                 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Country</label>
							    	<div class="col-sm-10">
							    	 <select id="country" name="country" class="form-control"></select>
							    	
							    		
							    		@if ($errors->has('country'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('country') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label" >State</label>
							    	<div class="col-sm-10">
							    	  <select id="state" name="state" class="form-control"></select> 

							    	 <!--  <input type="text" name="state" class="form-control" value="{{old('state')}}"> -->

							    		
							    		@if ($errors->has('state'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('state') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">City</label>
							    	<div class="col-sm-10">
							    	 <!--  <select id="state" name="state" class="form-control"></select> -->

							    	  <input type="text" name="city" class="form-control" value="{{old('city')}}">

							    		
							    		@if ($errors->has('city'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('city') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							    <div class="form-group">
							    	<label  class="col-sm-2 control-label">Post Box</label>
							    	<div class="col-sm-10">
							    	 <!--  <select id="state" name="state" class="form-control"></select> -->

							    	  <input type="text" name="postbox" class="form-control" value="{{old('postbox')}}"> (optional)

							    		
							    		@if ($errors->has('postbox'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('postbox') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>



                                 <div class="form-group">
							    	<label  class="col-sm-2 control-label">Telephone
                                    </label>
							    	<div class="col-sm-10">
							    		<input class="form-control tel" placeholder="Telephone" name="telephone" type="tel" value="{{old('telephone')}}">
							    		<span id="error-msg" class="hide">InValid</span>
							    		<span id="valid-msg" class="hide">Valid</span>
							    		@if ($errors->has('telephone'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('telephone') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Fax
                                    </label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Fax" name="fax" type="text" value="{{old('fax')}}"> (optional)
							    		
							    		@if ($errors->has('fax'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('fax') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>


					       <div class="form-group">
							    	<label  class="col-sm-2 control-label">Email</label>
							    	<div class="col-sm-10">
							    		<input class="form-control int" placeholder="Email" name="email" type="email" value="{{old('email')}}">
							    		
							    		@if ($errors->has('email'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('email') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							 </div>

							    
							     <div class="form-group">
							    	<label  class="col-sm-2 control-label">Site Url</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Site Url" name="site" type="text" value="{{old('site')}}">
							    		@if ($errors->has('site'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('site') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">Type(commerical)</label>
							    	<div class="col-sm-10">
							    		<textarea name="type" rows="2" class="form-control" >{{old('type')}}</textarea>
							    		@if ($errors->has('type'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('type') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>


                              <div class="form-group">
							    	<label  class="col-sm-2 control-label">Checks</label>
							    	<div class="col-sm-10">
							    		<textarea name="checks" rows="2" class="form-control" >{{old('checks')}}</textarea> (Optional)
							    		@if ($errors->has('checks'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('checks') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							      <div class="form-group">
							    	<label  class="col-sm-2 control-label">Test cells</label>
							    	<div class="col-sm-10">
							    		<input class="form-control" placeholder="Test cells" name="test_cell" type="text" value="{{old('test_cell')}}"> (Optional)
							    		@if ($errors->has('test_cell'))
							    		<span class="help-block">
							    			<strong>{{ $errors->first('test_cell') }}</strong>
							    		</span>
							    		@endif
							    	</div>
							    </div>

							<button type="submit" class="btn btn-primary sub">Save</button>

							<a href=" {{ url('admin/facility') }}"><button type="button" class="btn btn-primary">Back</button></a>

						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>

<style>
.intl-tel-input {
    width: 100%;
    margin-bottom: 2px;
}
</style>
@endsection



