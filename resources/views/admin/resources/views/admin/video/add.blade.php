@extends('layouts.adminapp')
@section('title', 'Add Video')
@section('content')
<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Add Video</h3>	
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Video</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Video</label>
								<div class="col-sm-10 drop">
									<input type="file" name="video" value="{{old('video')}}">
								</div>
								@if ($errors->has('video'))
								<span class="help-block">
									<strong>{{ $errors->first('video') }}</strong>
								</span>
								@endif
								<p class="text-center">-OR-</p>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Link</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Link" name="link" type="text" value="{{old('link')}}">
									@if ($errors->has('link'))
									<span class="help-block">
										<strong>{{ $errors->first('link') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
							<label for="input-success" class="col-sm-2 control-label">Description</label>
								<div class="col-sm-10">
									<textarea class="summernote" name="description">{{old('description')}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Insert Code" name="insert_code" type="text" value="{{old('insert_code')}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->

</div>
@endsection
@section('script')
<script>
	$('.summernote').summernote({
		height: 350
	});
</script>
@endsection