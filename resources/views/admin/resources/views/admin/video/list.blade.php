@extends('layouts.adminapp')
@section('title', 'Videos')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Videos</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Videos</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Name</th>
              <th>Link</th>
              <th>Video</th>
              <th>Insert Code</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($videos as $video)
            <tr>
              <td>{{ ucfirst($video->name) }}</td>
              <td><a target="_blank" href="{{$video->link}}">{{ $video->link}}</a></td>
              <td><!-- <a target="_blank" href="/video{{$video->video}}">{{ $video->video }}</a> -->
                  <video width="300" height="150" controls>
                  
                  <source src="{{ asset('video/'.$video->video) }}" type="video/mp4 .mp4">
                  <source src="{{ asset('video/'.$video->video) }}" type="video/3gp .3gp">
                   
                  Your browser does not support the video tag.
                  </video>
                <br> 
              </td>
              <td>{{ $video->insert_code }}</td>
                <td><a href="{{url('/admin/video/edit/'.$video->id)}}"><i class="fa fa-edit"></i></a><a class="deleteVideo" href="{{url('/admin/removeVideo/'.$video->id)}}"><i class="fa fa-remove"></i></a></td>
              </tr>
              @endforeach

            </tbody>
          </table>  
        </div>            
      </div>
    </div>
  </div>
</div><!-- Row -->
</div>
</div>
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteVideo').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this video.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection  