@extends('layouts.adminapp')
@section('title', 'Add Question')
@section('content')
<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Add Question</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Question</h4>
					</div>
					<div class="panel-body">
						<input type="radio" class="radio" value="simple" name="questionType">Simple
						<input type="radio" class="radio" value="multiple" name="questionType">Multiple
						<form style="display: none;" method="post" class="form-horizontal questionForm simple" action="{{url('/admin/question/add')}}">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Question</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Question" name="question" type="text" value="{{old('question')}}">
									@if ($errors->has('question'))
									<span class="help-block">
										<strong>{{ $errors->first('question') }}</strong>
									</span>
									@endif
									<input type="hidden" name="type" value="simple">
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Insert Code" name="insert_code" type="text" value="{{old('insert_code')}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<button type="submit" class="btn btn-primary" >Save</button>
						</form>
						<form style="display: none;" method="post" class="form-horizontal questionForm multiple" action="{{url('/admin/question/add')}}">
							{{ csrf_field() }}
							<div class="form-group">
								<label class="col-sm-2 control-label">Question</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Enter Question" name="question" type="text" value="{{old('question')}}">
									@if ($errors->has('question'))
									<span class="help-block">
										<strong>{{ $errors->first('question') }}</strong>
									</span>
									@endif
									<input type="hidden" name="type" value="multiple">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Options</label>
								<div class="col-sm-10 options">
									
									<div class="optionn">
										<input class="form-control" placeholder="Option" name="option[]" type="text">
									</div>
									<div class="optionn">
										<input class="form-control" placeholder="Option" name="option[]" type="text">
									</div>
									<div class="optionn">
										<input class="form-control" placeholder="Option" name="option[]" type="text">
									</div>
									<div class="optionn">
										<input class="form-control" placeholder="Option" name="option[]" type="text">
									</div>
									
									@if ($errors->has('option'))
									<span class="help-block">
										<strong>{{ $errors->first('option') }}</strong>
									</span>
									@endif
								</div>
								<i class="fa fa-plus addmore"></i>
							</div>
							<div class="form-group">
								<label  class="col-sm-2 control-label">Insert Code</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="insert Code" name="insert_code" type="text" value="{{old('insert_code')}}">
									@if ($errors->has('insert_code'))
									<span class="help-block">
										<strong>{{ $errors->first('insert_code') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->

</div>
@endsection
@section('script')
<script>
	$(".radio").click(function() {    
		$('.questionForm').slideUp('slow'); 
		$('.'+$(this).val()).slideDown('slow');
	});
	$('.addmore').click(function(){
		var option = "<div class='optionn'><input class='form-control' placeholder='Option' name='option[]' type='text'><i class='fa fa-minus removeOption'></i></div>";
		$('.optionn:last-child').after(option);
	});
	$(document).on('click','.removeOption',function(){
		$(this).parent('.optionn').remove();
	});

</script>
@endsection