@extends('layouts.adminapp')
@section('title', 'Questions')
@section('content')
<div class="page-inner" style="min-height:51px !important">
 <div class="page-title">
  <div class="container">
    <h3>Questions</h3>
  </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Questions</h4>
      </div>
      <div class="panel-body">
       <div class="table-responsive">
        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>Question</th>
              <th>Type</th>
              <th>Insert Code</th>
              <th>Created at</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($questions as $question)

            <tr>
              <td>{{ $question->question }}</td>
              <td>{{ ucfirst($question->type) }}</td>
              <td>{{ $question->insert_code }}</td>
              <td>{{ $question->created_at }}</td>
              <td><a href="{{url('/admin/question/edit/'.$question->id)}}"><i class="fa fa-edit"></i></a><a class="deleteQuestion" href="{{url('/admin/removeQuestion/'.$question->id)}}"><i class="fa fa-remove"></i></a></td>
            </tr>
            @endforeach

          </tbody>
        </table>  
      </div>
    </div>
  </div>
</div>
</div><!-- Row -->
</div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}"/>
@endsection  
@section('script')
<script type="text/javascript" src="{{asset('js/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $('.deleteQuestion').click(function(event) {
    var href = $(this).attr('href');
    event.preventDefault();
    swal({
      title: 'Are you sure?',
      text: "You want to delete this question.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      window.location = href;
    })
  });
</script>
@endsection