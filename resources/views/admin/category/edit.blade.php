@extends('layouts.adminapp')
@section('title', 'Edit Category')
@section('content')
<div class="page-inner" style="min-height:51px !important">

	<div class="page-title">
		<div class="container">
			<h3>Edit Category</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Edit Category</h4>
					</div>
					<div class="panel-body">
						{!! Form::open(array('method' => 'PUT' , 'enctype' => 'multipart/form-data')) !!}
						{{ csrf_field() }}
						<div class="form-group">
								<label  class="col-sm-2 control-label">Category Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Category Name" name="name" type="text" value="{{$category->name}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
							<a href="{{ url('admin/category') }}"><button type="button" class="btn btn-primary">Cancel</button></a>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div><!-- Row -->
</div><!-- Main Wrapper -->

</div>


@endsection
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
  </script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
  <script>        
            
       $(document).ready( function (){   
  
         $('.int').keypress(function(event) {
            	
                return isNumber(event, this);

           
            });

           function isNumber(evt, element) {

        	var charCode = (evt.which) ? evt.which : event.keyCode

       	 if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57)){

			$(element).parents('.form-group').find('.errRecord').text('Please fill only integer value.');
			            return false;
			}else{
				$('.errRecord').text('');
			        return true;
			    }
			    }  
          });
        
       </script>