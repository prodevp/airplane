@extends('layouts.adminapp')  
@section('title', 'Add Category')
@section('content')

<div class="page-inner" style="min-height:51px !important">
	<div class="page-title">
		<div class="container">
			<h3>Add Category</h3>
		</div>
	</div>
	<div id="main-wrapper" class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-heading clearfix">
						<h4 class="panel-title">Add Category</h4>
					</div>
					<div class="panel-body">
						<form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label  class="col-sm-2 control-label">Category Name</label>
								<div class="col-sm-10">
									<input class="form-control" placeholder="Category Name" name="name" type="text" value="{{old('name')}}">
									@if ($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							
							<button type="submit" class="btn btn-primary">Save</button>

							<a href=" {{ url('admin/category') }}"><button type="button" class="btn btn-primary">Back</button></a>

						</form>
					</div>
				</div>
			</div>
		</div><!-- Row -->
	</div><!-- Main Wrapper -->
</div>
@endsection


