<div class="navbar">
    <div class="navbar-inner container">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="{{ url('/home')}}" class="logo-text"><span>Airplane</span></a>
        </div><!-- Logo Box -->
        <div class="search-button">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
        </div>
        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>		
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <a href="#cd-nav" class="waves-effect waves-button waves-classic cd-nav-trigger"><i class="fa fa-diamond"></i></a>
                    </li>
                    <li>		
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-md dropdown-list theme-settings" role="menu">
                            <li class="li-group">
                                <ul class="list-unstyled">
                                    <li class="no-link" role="presentation">
                                        Fixed Header 
                                        <div class="ios-switch pull-right switch-md">
                                            <input type="checkbox" class="js-switch pull-right fixed-header-check" checked>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="li-group">
                                <ul class="list-unstyled">
                                    <li class="no-link" role="presentation">
                                        Fixed Sidebar 
                                        <div class="ios-switch pull-right switch-md">
                                            <input type="checkbox" class="js-switch pull-right fixed-sidebar-check">
                                        </div>
                                    </li>
                                    <li class="no-link" role="presentation">
                                        Toggle Sidebar 
                                        <div class="ios-switch pull-right switch-md">
                                            <input type="checkbox" class="js-switch pull-right toggle-sidebar-check">
                                        </div>
                                    </li>
                                    <li class="no-link" role="presentation">
                                        Compact Menu 
                                        <div class="ios-switch pull-right switch-md">
                                            <input type="checkbox" class="js-switch pull-right compact-menu-check" checked>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="no-link"><button class="btn btn-default reset-options">Reset Options</button></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>	
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <span class="user-name">{{Auth::user()->name}}<i class="fa fa-angle-down"></i></span>
                            <img class="img-circle avatar" src="{{ asset('images/avatar1.png') }}" width="40" height="40" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-list" role="menu">
                         {{--    <li role="presentation"><a href="{{ url('/admin/editprofile') }}"><i class="fa fa-user"></i>Edit Profile</a></li>
                            <li role="presentation"><a href="calendar.html"><i class="fa fa-calendar"></i>Calendar</a></li>
                            <li role="presentation"><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox<span class="badge badge-success pull-right">4</span></a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a href="lock-screen.html"><i class="fa fa-lock"></i>Lock screen</a></li> --}}
                            <li role="presentation">
                                <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out m-r-xs"></i> Log out
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic" id="showRight">
                            <i class="fa fa-comments"></i>
                        </a>
                    </li>
                </ul><!-- Nav -->
            </div><!-- Top Menu -->
        </div>
    </div>
</div><!-- Navbar -->
<div class="page-sidebar sidebar horizontal-bar">
    <div class="page-sidebar-inner">
        <ul class="menu accordion-menu">
            <li class="nav-heading"><span>Navigation</span></li>
            <li><a href="{{ url('/admin/dashboard') }}"><span class="menu-icon icon-speedometer"></span><p>Dashboard</p></a></li>
            <li class="droplink"><a href="{{ url('/admin/user') }}"><span class="menu-icon icon-users"></span><p>Users</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/adduser')}}">Add User</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="{{ url('/admin/facility') }}"><span class="menu-icon icon-users"></span><p>Facilities</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/addfacility')}}">Add Facilities</a></li>
                </ul>
            </li>

            <li class="droplink"><a href="{{ url('/admin/vendor') }}"><span class="menu-icon icon-users"></span><p>MRO Vendors</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/addvendor')}}">Add MRO Vendor</a></li>
                </ul>
            </li>


         <?php /*  <li class="droplink"><a href="{{url('/admin/pages/list')}}"><span class="menu-icon icon-docs"></span><p>Pages</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/pages/add')}}">Add Page</a></li>
                </ul>
            </li>

            <li class="droplink"><a href="{{ url('/admin/customers') }}"><span class="menu-icon icon-users"></span><p>Customers</p><span class="arrow"></span></a>
            </li>
            <li class="droplink"><a href="{{url('/admin/libraries')}}"><span class="menu-icon icon-book-open"></span><p>Library</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/library/add')}}">Add Library</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="{{url('/admin/questions')}}"><span class="menu-icon icon-question"></span><p>Questions</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/question/add')}}">Add Question</a></li>
                </ul>
            </li> 
            <li class="droplink"><a href="{{url('/admin/tiles')}}"><span class="menu-icon icon-layers icons"></span><p>Tiles</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/addTiles')}}">Add Tiles</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="{{url('/admin/products')}}"><span class="menu-icon icon-basket"></span><p>Products</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/product/add')}}">Add Product</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="{{url('/admin/videos')}}"><span class="menu-icon icon-camcorder"></span><p>Videos</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('/admin/video/add')}}">Add Video</a></li>
                </ul>
            </li>
            <li class="droplink"><a href="{{url('/admin/customer/contact')}}"><span class="menu-icon  icon-screen-smartphone "></span><p>Contact Us</p><span class="arrow"></span></a>
            </li>  
            */?>

                <li class="droplink"><a href="{{ url('admin/airPlanList') }}"><span class="menu-icon icon-users"></span><p>Airplane</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('admin/addPlane')}}">Create AirPlane Asset</a></li>

                </ul>
            </li>

 

                <li class="droplink"><a href="{{ url('admin/category') }}"><span class="menu-icon icon-users"></span><p>Category</p><span class="arrow"></span></a>
                <ul class="sub-menu">
                    <li><a href="{{url('admin/addCategory')}}">Add Category</a></li>

                </ul>
            </li>


                                        
        </ul>
    </div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->