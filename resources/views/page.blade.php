@extends('layouts.adminapp')
@section('title', $page->title)
@section('content')
<?php echo htmlspecialchars_decode($page->content); ?>
@endsection