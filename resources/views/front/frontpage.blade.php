<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Product Page</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
<div id="wrapper">
<section>
	<div class="container">
     	<div class="row tiles_sec">
        	<div class="col-xs-12">
				<div class="white_bg pl20 pr20 clearfix">
                <h2 class="tiles_heading">Ut labore labore et labore et dolore</h2>
               <div class="page-inner" style="min-height:51px !important">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                    <div class="tiles">
                    <!-- @foreach ($products as $product)
                     @if (file_exists(public_path('images/products/normal/'.$product->image)))
                     <a target="_blank" href="{{ asset('images/products/normal/'.$product->image) }}">
                     <img src="{{ asset('images/products/normal/'.$product->image) }}" width="300" height="300" alt=""></a>
                     @else
                     <a target="_blank" href="{{ $product->image }}">
                    <img src="{{ $product->image }}" width="300" height="300" alt=""></a>
                     @endif 
                    @endforeach -->
                      @foreach ($tiles as $tile)
                              
                            @if($tile->order_number == "1")<td>
                             <a target="_blank" href="{{$tile->link}}"> 
                             <?php if($tile->type == 'image'){ ?>
                             <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}" width="300" height="300" >
                             <?php  }else{ ?>{{ $tile->image }}
                             <?php } ?></a>
                             </td> @endif
                             @endforeach
                        </div><!-- tiles -->
                    </div><!-- col-xs-12 -->
                    <div class="col-xs-12 col-sm-4">
                        <div class="tiles">

                        @foreach ($tiles as $tile)
                             
                            @if($tile->order_number == "2")<td>
                             <a target="_blank" href="{{$tile->link}}"> 
                             <?php if($tile->type == 'image'){ ?>
                             <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}" width="300" height="300" >
                             <?php  }else{ ?>{{ $tile->image }}
                             <?php } ?></a>
                             </td>@endif
                             @endforeach
                        </div><!-- tiles -->
                    </div><!-- col-xs-12 -->
                    <div class="col-xs-12 col-sm-4">
                        <div class="tiles">
                            @foreach ($tiles as $tile)
                              
                            @if($tile->order_number == "3")<td>
                            <a target="_blank" href="{{$tile->link}}"> 
                             <?php if($tile->type == 'image'){ ?>
                             <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}"  width="300" height="300" >
                             <?php  }else{ ?>{{ $tile->image }}
                             <?php } ?></a>
                             </td>@endif
                             @endforeach
                        </div><!-- tiles -->
                    </div><!-- col-xs-12 -->
                </div><!-- row -->
				</div>
            </div><!-- col-xs-12 -->
        </div><!-- row -->
        <div class="row">
        	<div class="col-xs-12 col-sm-4">
            	<div class="tiles">
        		@foreach ($tiles as $tile)
                @if($tile->order_number == "4")<td>  
                 <a target="_blank" href="{{$tile->link}}"> 
                 <?php if($tile->type == 'image'){ ?>
                <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}"  width="300" height="300" >
                <?php  }else{ ?>{{ $tile->image }}
                <?php } ?></a>
                </td>@endif
                @endforeach
 		        </div><!-- tiles -->
            </div><!-- col-xs-12 -->
        	<div class="col-xs-12 col-sm-4">
            	<div class="tiles text-center">
        			<div class="code clearfix white_bg">
                       @foreach ($promocode as $promo)

                        @if($pd=$promo->promo_code)
                        <h3>{{$promo->promo_code}}</h3>
                        @endif       
                       @endforeach
                    	 
                        <p>save 10% <span>when using this code</span></p>
                    </div><!-- code -->
                    <div class="social_media clearfix white_bg">
                    	<h3>Follow us on</h3>
						<ul class="social_icons">
                        	<li><a href="http://www.facebook.com"><i class="fa fa-facebook dark_blue" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com"><i class="fa fa-twitter light_blue" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com"><i class="fa fa-instagram black" aria-hidden="true"></i></a></li>
							<li><a href="https://plus.google.com/"><i class="fa fa-google-plus orange" aria-hidden="true"></i></a></li>
							<li><a href="https://youtube.com"><i class="fa fa-youtube-play red" aria-hidden="true"></i></a></li>
							<li><a href="https://in.linkedin.com/"><i class="fa fa-linkedin blue" aria-hidden="true"></i></a></li>
                        </ul><!-- social_icons -->
                    </div><!-- social_media -->
 		        </div><!-- tiles -->
            </div><!-- col-xs-12 -->
        	<div class="col-xs-12 col-sm-4">
            	<div class="tiles">
                    @foreach ($tiles as $tile)
                            @if($tile->order_number == "5")<td> 
                            <a target="_blank" href="{{$tile->link}}">
                             <?php if($tile->type == 'image'){ ?>
                             <img src="{{ asset('images/tiles/200by200/'.$tile->image) }}"  width="300" height="300" >
                             <?php  }else{ ?>{{ $tile->image }}
                             <?php } ?></a>
                             </td>@endif
                             @endforeach
 		        </div><!-- tiles -->
            </div><!-- col-xs-12 -->
        </div><!-- row -->
        <div class="row">
        	<div class="col-xs-12">
                <div class="video_sec">
                <td>
                 @foreach ($videos as $video)
                  
                  <video width="1200" height="400" controls align="center" >
                  
                  <source src="{{ asset('video/'.$video->video) }}" type="video/mp4 .mp4">
                  <source src="{{ asset('video/'.$video->video) }}" type="video/3gp .3gp">
                   
                  Your browser does not support the video tag.
                  </video>
                <br> 
              @endforeach
                </td>
                </div><!-- video_sec -->
            </div><!-- col-xs-12 -->
        </div><!-- row -->
        <div class="row">
        	<div class="col-xs-12 col-sm-6">
            	<div class="faq white_bg pl20 pr20 clearfix">
                	<h3 class="text-uppercase">FAQ</h3>
                  <div class="list-group">
                     @foreach ($pages as $page)
                     <tr>
                       <td><p>{{html_entity_decode($page->content)}}</p></td> 
                     </tr>  
                    @endforeach
                 </div>
                </div><!-- faq -->
            </div><!-- col-sm-6 -->
            <div class="col-xs-12 col-sm-6">
            	<div class="contact white_bg pl20 pr20 clearfix">
                	<h3 class="text-uppercase">GET in touch with us</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed does eiusmodes tempor incididunt ut labore labore et labore et dolore magna aliqua. Ut enim ad minim venim.</p>

                    <form method="post" class="form-horizontal dropzone" id="my-awesome-dropzone" action="" >
                            {{ csrf_field() }}
                    	   <div class="form-group">
                        	<input placeholder="Name" type="text" class="form-control" name="name" />
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                    	   <div class="form-group">
                        	<input placeholder="Email" type="email" class="form-control" name="email" />
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif  
                            </div>
                    	    <div class="form-group">
                        	<textarea placeholder="Your message" type="text" class="form-control" name="message"></textarea>
                            @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                           </div>
                    	   <div class="form-group">
                        	<button type="submit" class="btn text-uppercase">Send message</button>
                        </div>
                    </form>
                </div><!-- contact -->
            </div><!-- col-sm-6 -->
        </div><!-- row -->
    </div><!-- container -->
</section>
<footer>
</footer>
</div><!--wrapper-->
</body>
</html>


