<!DOCTYPE html>
<html>
<head>
    <!-- Title -->
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="rexweb" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="siteurl" content="http://112.196.42.180/projects/amazoncustomer/amazoncustomer/public/">
    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
    <link href="{{ asset('plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/weather-icons-master/css/weather-icons.min.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/metrojs/MetroJs.min.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>	
    <link href="{{ asset('plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>  
    <link href="{{ asset('plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>
    @yield('style')
    <!-- Theme Styles -->
    <link href="{{ asset('css/modern.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/summernote.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/summernote-bs2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/summernote-bs3.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
    <script src="{{ asset('plugins/jquery/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="page-header-fixed compact-menu page-horizontal-bar">
        <div class="overlay"></div>
        @include('includes.appheader')
        <main class="page-content content-wrap">
            @include('includes.navbar')
            @yield('content')
        </main><!-- Page Content -->
        <nav class="cd-nav-container" id="cd-nav">
            <header>
                <h3>Navigation</h3>
                <a href="#0" class="cd-close-nav">Close</a>
            </header>
            <ul class="cd-nav list-unstyled">
                <li class="cd-selected" data-menu="index">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-home"></i>
                        </span>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li data-menu="profile">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <p>Profile</p>
                    </a>
                </li>
                <li data-menu="inbox">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-envelope"></i>
                        </span>
                        <p>Mailbox</p>
                    </a>
                </li>
                <li data-menu="#">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-tasks"></i>
                        </span>
                        <p>Tasks</p>
                    </a>
                </li>
                <li data-menu="#">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-cog"></i>
                        </span>
                        <p>Settings</p>
                    </a>
                </li>
                <li data-menu="calendar">
                    <a href="javsacript:void(0);">
                        <span>
                            <i class="glyphicon glyphicon-calendar"></i>
                        </span>
                        <p>Calendar</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="cd-overlay"></div>


        <!-- Javascripts -->

        <script src="{{ asset('plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('plugins/classie/classie.js') }}"></script>
        <script src="{{ asset('plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('plugins/3d-bold-navigation/js/main.js') }}"></script>
        <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.symbol.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('plugins/curvedlines/curvedLines.js') }}"></script>
        <script src="{{ asset('plugins/metrojs/MetroJs.min.js') }}"></script>
        <script src="{{ asset('js/modern.min.js') }}"></script>

        <script src="{{ asset('js/summernote.js') }}"></script>
        <script src="{{ asset('js/summernote.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/js/jquery.datatables.min.js') }}"></script>
        @yield('script')
        <script> $('#example').DataTable({
         "ordering": false
     });
        var pgurl = window.location.href.substr(window.location.href);
        $(".menu  li a").each(function() {
            if ($(this).attr("href") == pgurl || $(this).attr("href") == ''){
                $(this).addClass("active");

                $(this).parent('li').addClass('active');
            }
        });
    </script>
    @if (session('status'))
    <script type="text/javascript">

        setTimeout(function() {
            toastr.options = {
              "closeButton": false,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-center",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
          toastr.success("{{ session('status') }}");
      }, 500);


  </script>
  @endif
</body>
</html>