  $(document).ready( function (){
 populateCountries("country", "state");
  /*********phone******/
   $(".tel").intlTelInput();
   var getCode = $(".tel").intlTelInput('getSelectedCountryData').dialCode;
var teleph = $('.teleph').val();
if(teleph){
   $('.tel').val(teleph);
}else{
    $('.tel').val('+' + getCode);
}
    /*************For phone valid******/
    var telInput = $(".tel"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");
    // initialise plugin
    telInput.intlTelInput({
        utilsScript: "../js/utils.js"
    });
    var reset = function() {
        telInput.removeClass("error");
        errorMsg.addClass("hide");
        validMsg.addClass("hide");
    };
    // on blur: validate
    telInput.blur(function() {
        reset();
        if ($.trim(telInput.val())) {
            //alert($.trim(telInput.val()));
            if (telInput.intlTelInput("isValidNumber")) {
                validMsg.removeClass("hide");
            } else {
                telInput.addClass("error");
                errorMsg.removeClass("hide");
            }
        }
    });
    // on keyup / change flag: reset
    telInput.on("keyup change", reset);
    /************end***********/
    $(".tel").on("blur keyup change", function() {
        if ($(this).val() == '') {
            var getCode = $(".tel").intlTelInput('getSelectedCountryData').dialCode;

            $(this).val('+' + getCode);

        }
    });
        if ($(".tel").val() == '') {
            var getCode = $("#mobile-number").intlTelInput('getSelectedCountryData').dialCode;
            localStorage.setItem('code', getCode); 
                        $(".tel").val('+' + getCode);
        }
    
    /********phone********/
/***vendor form validation **/

$('.vendor').on('submit',function (event){
  
var valid = $(".vendor").valid();
        if (valid) {
            
               if ($('.tel').intlTelInput("isValidNumber")) {
                
            } else {
                $('.teErr').show();
                $('.tel').focus();
                event.preventDefault();
            }

        } else {
            
            event.preventDefault();
        }
    });

$('.vendor').validate({
        rules: {
            'name':'required',
            'company_name': 'required',
            'address': 'required',
             'country': 'required',
             'state': 'required',
            'city': 'required',
             'facility': 'required',
             "email": {
                required: true,
                email: true
            },
             'telephone': 'required',
               "site": {
                required: true,
                url: true
            },
             
        },
        messages: {
             name: {
                required: "Name can't be blank"
            },
            company_name: {
                required: "Company name can't be blank"
            },
            address: {
                required: "Address can't be blank"
            },
            country: {
                required: "Country can't be blank"
            },
            state: {
                required: "State can't be blank"
            },
            city: {
                required: "City can't be blank"
            },
           
             facility: {
                required: "Facility can't be blank"
            },
             email: {
                required: "Email can't be blank",
                email:"Email is not valid"
            },
            telephone: {
                required: "Telephone can't be blank"
            },
            site: {
                required: "Site can't be blank",
                url: "Please share valid url"
            }
             
        }

    });

/**end of vendor form validation***/

/**facility validation ***/
$('.facility').on('submit',function (event){

        var valid = $(".facility").valid();
        if (valid) {
           
               if ($('.tel').intlTelInput("isValidNumber")) {
                
            } else {
                $('.teErr').show();
                $('.tel').focus();
                 $('html,body').animate({
        scrollTop: $(".tel").offset().top},
        'slow');

                event.preventDefault();
            }

        } else {
            
            event.preventDefault();
        }
    });



    $('.facility').validate({
        rules: {
            'name':'required',
            'company_name': 'required',
            'address': 'required',
             'country': 'required',
             'state': 'required',
            'city': 'required',
             'facility': 'required',
             "email": {
                required: true,
                email: true
            },
             'telephone': 'required',
               "site": {
                required: true,
                url: true
            },
               'type': 'required'

        },
        messages: {
             name: {
                required: "Name can't be blank"
            },
            company_name: {
                required: "Company name can't be blank"
            },
            address: {
                required: "Address can't be blank"
            },
            country: {
                required: "Country can't be blank"
            },
            state: {
                required: "State can't be blank"
            },
            city: {
                required: "City can't be blank"
            },
           
             facility: {
                required: "Facility can't be blank"
            },
             email: {
                required: "Email can't be blank",
                email:"Email is not valid"
            },
            telephone: {
                required: "Telephone can't be blank"
            },
            site: {
                required: "Site can't be blank",
                url: "Please share valid url"
            },
             type: {
                required: "Type can't be blank"
            }
        }

    });
     });

 