	var id = $('.sta_id').text();
	
	$('.edit_rate').on('click',function(){
		//var type = $('.staff_type_name').text();
		setInput();
		
	});

	function changeInput(){
		setInput();
	};

	function setInput(type){
		var type = $('.rateValue').val();		
		$('.stafftype_select').show();
		$('.staff_type').hide();
		var val = $('.type_val').val();
		/*if (val == null){
			type = 0;
		}*/
		var input = '';
		console.log(val);
		if(type == 'No pay'){
			$('.typeInput').hide();
		}else if(type == 'Flat Rate'){
			input = '<div class="input-group m-b-sm"><span class="input-group-addon" id="basic-addon1">$</span><input class="form-control type_val" value="'+val+'" placeholder="Enter Price" aria-describedby="basic-addon1" type="text"></div>';
		}else if(type == 'Percentage Rate'){
			input = '<div class="input-group m-b-sm"><input class="form-control type_val" placeholder="Enter Percentage" aria-describedby="basic-addon1" value="'+val+'" type="text"><span class="input-group-addon" id="basic-addon1">%</span></div>';
		}

		//var html = input+'<a href="javascript:void(0)" onclick="savePayRate('+id+')" class="btn btn-success">Save</a>';
		$('.staff_input').html(input);
		//$('.rateValue').val(type);
	}

		function savePayRate(){
			var type = $('.rateValue').val();
			var typeval = $('.type_val').val();
			if(type == 'No pay'){
				typeval = 0;
			}
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }); 
            $.post("/admin/staffappointment",
                        {
                            sid: id,
                            type:type,
                            typeval:typeval
                        },
                        function (data) {
                            $('.stafftype_select').hide();
                            $('.staff_type').show();
                            $('.staff_value').html(data);
                        });
		}