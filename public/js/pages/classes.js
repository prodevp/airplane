$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }); 

function addNewServiceCategory(){
	$.ajax({
		url: '/admin/addServiceCategory',
		type: 'POST',
	})
	.done(function(e) {
		$('#accordion2').append(e);
		$('#accordion2 > div:last-child').show('slow');
	});
	
}