<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $guarded = ['_token'];

	public function options(){
		return $this->hasMany('App\QuestionOption');
	}

	protected static function boot() {
		parent::boot();

        static::deleting(function($question) { // before delete() method call this
        	$question->options()->delete();
             // do the rest of the cleanup...
        });
    }
}