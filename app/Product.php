<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
     protected $fillable = [
        'name', 'description', 'insert_code','image','filename','sku'
    ];
	
}
