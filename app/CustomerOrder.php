<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    //
	protected $fillable = ['asin','title','promotion_id','customer_id'];
}