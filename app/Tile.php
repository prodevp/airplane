<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tile extends Model
{
	protected $fillable = ['name','type','link','insert_code','image','all_codes','position','sku'];

}