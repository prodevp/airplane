<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Library;
use Newsletter;

class LibraryController extends Controller
{
//*************************************************************************************// 
    public function add(Request $request){
    // Newsletter::subscribe('surjeetpatran@gmail.com.com');
        if ($request->isMethod('post')) {
            $ebook = $request->all();
            $validator = Validator::make($ebook, [
                'ebook' => 'required',
                'name'  => 'required',
                'insert_code'  => 'required',
                ])->validate();
            
            if ($request->file('ebook')) {
                $extension = $request->ebook->extension();
                $library = new Library;
                $library->name = $ebook['name'];
                $library->description = $ebook['description'];
                $library->insert_code = $ebook['insert_code'];
                $library->filename =  Storage::disk('ebook')->put('', $request->file('ebook'));

                $library->save();
                $request->session()->flash('status', 'Library Added Successfully'); 
                return redirect('admin/libraries');
            }

        }else{
            return view('admin/library/add');
        }

    }
//*************************************************************************************// 
    public function listEbook(){
        $libraries = Library::all();
        return view('admin/library/list',compact('libraries'));
    }
//*************************************************************************************// 
    public function downloadLibrary($id){
        $library = Library::find($id);
        return response()->download(public_path().'/ebook/'.$library['filename']);
    }
//*************************************************************************************// 
    public function removeLibrary(Request $request,$id){
        $library = Library::find($id);
        unlink(public_path().'/ebook/'.$library['filename']);
        $library->delete();
        $request->session()->flash('status', 'Library Deleted Successfully');
        return redirect('admin/libraries');
    }
//*************************************************************************************// 
    public function edit(Request $request,$id){
        $library = Library::find($id);
        if ($request->isMethod('PUT')) {
            $requestebook = $request->except(['_method', '_token']);
            $libraryEdit = array();
            Validator::make($requestebook, [
                'name'  => 'required',
                'insert_code'  => 'required'
                ])->validate();
            if ($request->file('ebook')) {
                $extension = $request->ebook->extension();                                
                $libraryEdit['filename'] =  Storage::disk('ebook')->put('', $request->file('ebook'));
            }else{
                $libraryEdit['filename'] = $library['filename'];
            }
            $libraryEdit['name'] = $requestebook['name'];
            $libraryEdit['description'] = $requestebook['description'];
            $libraryEdit['insert_code'] = $requestebook['insert_code'];
            Library::where('id', $id)
            ->update($libraryEdit);
            $request->session()->flash('status', 'Library Edited Successfully');
            return redirect('admin/libraries');

        }else{    
         return view('admin/library/edit',compact('library'));
     }
 }
//*************************************************************************************//  
}
