<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Video;

class VideoController extends Controller
   {
//*************************************************************************************// 	
    public function add(Request $request){

        if ($request->isMethod('post')) {
            $video = $request->all();
            $validator = Validator::make($video, [
                'name'  => 'required',
              //  'video'  => 'required',
                'insert_code'  => 'required',
                ])->validate();

          		$Video = new Video;
       
		        if ($request->file('video')) {
		                $extension = $request->video->extension();
		                $Video->video =  Storage::disk('video')->put('', $request->file('video'));
		        }
            
                $Video->name = $video['name'];
                $Video->link = $video['link'];
                $Video->description = $video['description'];
                $Video->insert_code = $video['insert_code'];
                 $Video->sku = $video['sku'];
                $Video->save();

                $request->session()->flash('status', 'Video Added Successfully');
                return redirect('admin/videos');
               
               }else{
 	            return view('admin/video/add');
               }
            }
        
//*************************************************************************************// 
    public function videoList(){
        $videos = Video::all();
        return view('admin/video/list',compact('videos'));
    }
//*************************************************************************************//
    public function edit(Request $request,$id){
        $video = Video::find($id);
         if ($request->isMethod('PUT')) {
            $requestvideo = $request->except(['_method', '_token']);
            $videoEdit = array();
             Validator::make($requestvideo, [
                'name'  => 'required',
                'insert_code'  => 'required'
                 ])->validate();
            if ($request->file('video')) {
                $extension = $request->video->extension();                                
                $videoEdit['video'] =  Storage::disk('video')->put('', $request->file('video'));
             }
             // else{
            //     $videoEdit['video'] = $requestvideo['video'];
            // }
            $videoEdit['name'] = $requestvideo['name'];
            $videoEdit['description'] = $requestvideo['description'];
            $videoEdit['insert_code'] = $requestvideo['insert_code'];
            Video::where('id', $id)
            ->update($videoEdit);
            $request->session()->flash('status', 'Video Edited Successfully');

              return redirect('admin/videos');

         }else{    
          return view('admin/video/edit',compact('video'));
     }
  }
    
//*************************************************************************************//

 
    public function removeVideo(Request $request,$id){
		$videos = Video::find($id);
		$videos->delete();
		$request->session()->flash('status', 'Video Deleted Successfully');
		return redirect('admin/videos');
	}
//*************************************************************************************//    
}