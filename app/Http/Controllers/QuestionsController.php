<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Question;
use App\QuestionOption;

class QuestionsController extends Controller
{
//*************************************************************************************// 
	public function add(Request $request){
		if ($request->isMethod('post')) {
			$requestQuestion = $request->all();
			if($requestQuestion['type'] == 'simple'){
				Validator::make($requestQuestion, [
					'question' => 'required',
					'insert_code' => 'required',
					])->validate();
				$question = $requestQuestion['insert_code'];
				$question = Question::create($requestQuestion);

			}else{
				Validator::make($requestQuestion, [
					'question' => 'required',
					'option'   => 'required',
					'insert_code' => 'required',
					])->validate();
				$question = $request->except(['option']);
				$question = Question::create($question);
				for($i=0;$i<count($requestQuestion['option']);$i++){
					if($requestQuestion['option'][$i] != ''){
						$options = new QuestionOption;
						$options->question_id = $question['id'];
						$options->question_option = $requestQuestion['option'][$i];
						$options->save();
					}
					
				}
			} 
			$request->session()->flash('status', 'Question Added Successfully');
			return redirect('admin/questions');
		}else{
			return view('admin/question/add');
		}

	}
//*************************************************************************************// 
	public function listQuestions(){
		$questions = Question::all();
		return view('admin/question/list',compact('questions'));

	}
//*************************************************************************************// 
	public function edit(Request $request,$id){
		$question = Question::with('options')->find($id);
		if ($request->isMethod('PUT')) {
			$requestQuestion = $request->except(['_method', '_token']); 
			Validator::make($requestQuestion, [
				'question' => 'required',
				'insert_code' => 'required',
				])->validate();
			if($requestQuestion['type'] == 'simple'){
				$question = Question::where('id', $id)
				->update($requestQuestion);

			}else{
				$question = $request->except(['_method','_token','option']);
				$question = Question::where('id', $id)
				->update($question);
				$options = QuestionOption::where('question_id',$id)->delete();
				for($i=0;$i<count($requestQuestion['option']);$i++){
					if($requestQuestion['option'][$i] != ''){
						$options = new QuestionOption;
						$options->question_id = $id;
						$options->question_option = $requestQuestion['option'][$i];
						$options->save();
					}
				}
			} 			
			$request->session()->flash('status', 'Question Edited Successfully');
			return redirect('admin/questions');
		}else{
			return view('admin/question/edit',compact('question'));
		}
	}
//*************************************************************************************// 
	public function removeQuestion(Request $request,$id){
		$question = Question::find($id);
		$question->delete();
		$request->session()->flash('status', 'Question Deleted Successfully');
		return redirect('admin/questions');
	}
//*************************************************************************************// 	
}