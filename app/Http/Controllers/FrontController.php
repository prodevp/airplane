<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Product;
use App\Tile;
use App\Page;
use App\PromoCode;
use App\InsertCode;
use App\BigDiscountCode;
use App\User;
use Validator;
use Peron\AmazonMws\AmazonOrderItemList; 
use App\Message;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
   {
//***************************************************************************//
   public function view($slug){

  } 
//***************************************************************************//    	
   public function FrontEnd(){

     return view('front/frontpage');
 
   }
//***************************************************************************//
   public function front_end(){

    return view('front/frontpages');
   }
//***************************************************************************//   
   public function product_list(Request $request){
   	   
       $findids     = $request->session()->get('promo_code');
       $insert_code = $request->session()->get('insert_code');
       $videos      = Video::where('insert_code', $insert_code)->get();
       $products    = Product::where('insert_code', $insert_code)->get();
       $tiles       = Tile::where('insert_code', $insert_code)->get();
       $pages       = Page::select('content')->get();
       $promocode   = User::where('promo_code',$findids)->get();
        // $id_get      = $promocode->email;
        
       return view('front/frontpage',compact('videos','products','tiles','pages','promocode'));
    } 

 
//***************************************************************************//   
    public function add(Request $request){
     
        if ($request->isMethod('post')) {
            $message = $request->all();
            $validator = Validator::make($message, [
                'name' => 'required',
                'email'  => 'required',
                'message'  => 'required',
                ])->validate();

            $products = new Message;
            $products->name = $message['name'];
            $products->email = $message['email'];
            $products->message = $message['message'];
            $products->save();
            
            $request->session()->flash('status', 'Message Sent Successfully!'); 
             
             return redirect('/frontpage');
            
          }else{

              return view('front/frontpage');
         }
    }
//***************************************************************************//
    public function order_list(Request $request){
      $order_number = $request->session()->get('order_number');
      $findids     = $request->session()->get('promo_code');
      $order_items = new AmazonOrderItemList("Slope Bags");
      $order_items->setUseToken();
      $order_items->setOrderId($order_number);
      $order_items->fetchItems();
      $customer_orders = $order_items->getItems();
      $promocode   = User::where('promo_code',$findids)->get();
      $product1    = Product::inRandomOrder()->limit(1)->get();
      $product2    = Product::inRandomOrder()->limit(1)->get();
      $product3    = Product::inRandomOrder()->limit(1)->get();
      $product4    = Product::inRandomOrder()->limit(1)->get();
      $product5    = Product::inRandomOrder()->limit(1)->get();
      $videos      = Video::inRandomOrder()->limit(1)->get();
      $pages       = Page::select('content')->get();
       
      // echo "<pre>"; print_r($products); die();
      return view('front/frontpages',compact('customer_orders','promocode','product1','product2','product3',
        'product4','product5','videos','pages'));
    }

//***************************************************************************//
}
