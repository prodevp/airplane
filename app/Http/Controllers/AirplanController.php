<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StaffRequest;
use App\User;
use App\StaffInfo; 
use DB;
use Image; 
use Carbon\Carbon;
use Validator;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Peron\AmazonMws\AmazonOrderList; 
use Peron\AmazonMws\AmazonOrderItemList; 
use App\Airplan;
use App\Category;
use App\Facility;
use Illuminate\Support\Facades\Storage;
use App\CustomerOrder;
use File;
use Newsletter;
use View;
use Barryvdh\DomPDF\Facade as PDF;
use NZTim\Mailchimp\Mailchimp;

class AirplanController extends Controller
{

//***************************************************************************//
  protected function getSavedImageName( $timestamp, $image )
  {
    return $timestamp . '-' . $image->getClientOriginalName();
  }
//***************************************************************************//
  protected function getFormattedTimestamp()
  {
    return str_replace( [' ', ':'], '-', Carbon::now()->toDateTimeString() );
  }
//***************************************************************************//
  
     /********new********/
 public function addPlans(Request $request){
  $category = Category::all();
    $facility = Facility::all();
       if ($request->isMethod('post')) {

        $userId = Auth::id();


            $product_data = $request->all();
            
                 $validator = Validator::make($product_data, [
                'airType'  => 'required',
                'variable'  => 'required',
               'MSN'  => 'required',
                'ESN'  => 'required',
                'utilisation'  => 'required',
                'cycle'  => 'required',
                'engineType'  => 'required',
                'timeTaken'  => 'required',
                'category'  => 'required',
                'TT'  => 'required',
                'TC'  => 'required',
                'facility'  => 'required',
                'manufacture'  => 'required',
                 'airVariant'  => 'required',
                  'engin_TC'  => 'required',
                  'engin_TT'  => 'required',
                  'TSLSV'  => 'required',
                  'CSLSV'  => 'required',
                   'reason'  => 'required',

                            ])->validate();
            $cat = json_encode($product_data['category']);
            if($request->file('image')){
                $image = $request->file('image');            
                $filesname  = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('images/airplan/'.$filesname);
                Image::make($image->getRealPath())->save($path);
            
           // echo'<pre>';print_r($product_data);die('a');
        $insertcode = DB::table('airplans')->insert(
    ['airVariant' => $product_data['airVariant'], 'airType' => $product_data['airType'],'image'=>$filesname,'variable'=>$product_data['variable'],'MSN'=>$product_data['MSN'],'ESN'=>$product_data['ESN'],'utilisation'=>$product_data['utilisation'],'cycle'=>$product_data['cycle'],'engineType'=>$product_data['engineType'],'timeTaken'=>$product_data['timeTaken'],'category'=>$cat,'TT'=>$product_data['TT'],'TC'=>$product_data['TC'],'facility'=>$product_data['facility'],'manufacture'=>$product_data['manufacture'],'engin_TC'=>$product_data['engin_TC'],'engin_TT'=>$product_data['engin_TT'],'TSLSV'=>$product_data['TSLSV'],'CSLSV'=>$product_data['CSLSV'],'reason'=>$product_data['reason'],'bought'=>$product_data['bought'],'price'=>$product_data['price'],'userId'=>$userId ]
      );  

       
           if($insertcode){
               
                $request->session()->flash('status', 'Airplane Added Successfully'); 
                return redirect('admin/airPlanList');
              } 
              }
            
        }else{
            return view('admin/airplan/add',compact('category','facility'));
       }
    }

    /*****************list all airCraft*************/
     public function airJetlist(){
      $products = Airplan::orderBy('created_at', 'desc')->get();
       // $products = Airplan::all()->orderBy('created_at', 'desc');
        return view('admin/airplan/list',compact('products'));
    }


    public function editPlan(Request $request,$id){
        $category = Category::all();
        $facility = Facility::all();
            $product = Airplan::find($id);
            //echo "<pre>"; print_r($product); die();
            if ($request->isMethod('PUT')) {
                $requestProduct = $request->except(['_method', '_token']);
                $libraryEdit = array();

                 $validator = Validator::make($requestProduct, [
                'airType'  => 'required',
                'variable'  => 'required',
               'MSN'  => 'required',
                'ESN'  => 'required',
                'utilisation'  => 'required',
                'cycle'  => 'required',
                'engineType'  => 'required',
                'timeTaken'  => 'required',
                'category'  => 'required',
                'TT'  => 'required',
                'TC'  => 'required',
                'facility'  => 'required',
                'manufacture'  => 'required',
                 'airVariant'  => 'required',
                   'engin_TC'  => 'required',
                     'engin_TT'  => 'required',
                       'TSLSV'  => 'required',
                         'CSLSV'  => 'required',
                           'reason'  => 'required',
                            ])->validate();
               
               if($request->file('image')){
                $image = $request->file('image');
                $filename  = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('images/airplan/'.$filename);
                Image::make($image->getRealPath())->save($path);
                  
              if($filename){
               $requestProduct['image'] = $filename;
              }         
                }
                  $cat = json_encode($requestProduct['category']);
                  $requestProduct['category'] = $cat;
                $product = Airplan::where('id', $id)
                ->update($requestProduct);


                $request->session()->flash('status', 'Airplane Edited Successfully');
                return redirect('admin/airPlanList');
            
        }else{       
             return view('admin/airplan/edit',compact('product','category','facility'));
           }
        }


 
//***************************************************************************//
   public function removeAirCraft(Request $request,$id){
    $plane = Airplan::find($id);
  $image_path = public_path('images/airplan/'.$plane['image']);
if(File::exists($image_path)) {
    File::delete($image_path);
    
}
$plane->delete();
    


   

    $request->session()->flash('status', 'Airplane Deleted Successfully');
    return redirect('admin/airPlanList');
  }

  /********View**********/
   public function viewAirCraft(Request $request,$id){
    $product = Airplan::find($id);
    return view('admin/airplan/profile',compact('product'));
  }


/***********category***************/

    public function category(){
            $category = Category::orderBy('created_at', 'desc')->get();

      //  $category = Category::all();
        return view('admin/category/list',compact('category'));
    }


//remove category
    public function removeCategory(Request $request,$id){
      $member = Category::find($id);
      $member->delete();
      $request->session()->flash('status', 'Category Deleted Successfully');
      return redirect('admin/category');
  }

    public function editCategory(Request $request,$id){
            $category = Category::find($id);
            //echo "<pre>"; print_r($product); die();
            if ($request->isMethod('PUT')) {
                $requestProduct = $request->except(['_method', '_token']);
                $libraryEdit = array();

                 $validator = Validator::make($requestProduct, [
               'name'  => 'required|unique:categories',
                            ])->validate();
                
                $product = Category::where('id', $id)
                ->update($requestProduct);


                $request->session()->flash('status', 'Category Edited Successfully');
                return redirect('admin/category');
            
        }else{       
             return view('admin/category/edit',compact('category'));
           }
        }


    public function addCategory(Request $request){
       if ($request->isMethod('post')) {

            $product_data = $request->all();
            
                 $validator = Validator::make($product_data, [
                'name'  => 'required|unique:categories',

                ])->validate();
            
        $insertcode = DB::table('categories')->insert(
    ['name' => $product_data['name']]
      );  

       
           if($insertcode){
               
                $request->session()->flash('status', 'Category Added Successfully'); 
                return redirect('admin/category');
              } 
              
            
        }else{
            return view('admin/category/add');
       }
    }


/**************end of category*********/


}
