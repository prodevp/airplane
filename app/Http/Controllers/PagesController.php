<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Page;
use Image;
use App\Tile;
use Newsletter;

class PagesController extends Controller
{
//*************************************************************************************// 
	public function view($slug){

	}
//*************************************************************************************// 
	public function add(Request $request){
		if ($request->isMethod('post')) {
			$requestpage = $request->all();
			Validator::make($requestpage, [
				'title' => 'required',
				'slug'  => 'required',
				'content' => 'required'
				])->validate();
			$requestpage['content'] = htmlspecialchars($requestpage['content']);
			$page = Page::create($requestpage);
			$request->session()->flash('status', 'Page Added Successfully');
			return redirect('admin/pages/list');
		}else{
			return view('admin/pages/add');
		}
		
	}
//*************************************************************************************// 
	public function listpages(){
		$pages = Page::all();
		return view('admin/pages/list',compact('pages'));
	}
//*************************************************************************************// 
	public function edit(Request $request,$id){
		$method = $request->method();
		if ($request->isMethod('PUT')) {
			$requestpage = $request->except(['_method', '_token']); 
			Validator::make($requestpage, [
				'title' => 'required',
				'slug'  => 'required',
				'content' => 'required'
				])->validate();
			$requestpage['content'] = htmlspecialchars($requestpage['content']);
			Page::where('id', $id)
			->update($requestpage);
			$request->session()->flash('status', 'Page Edited Successfully');
			return redirect('admin/pages/list');
		}else{
			$page = Page::find($id);
			return view('admin/pages/edit',compact('page'));
		}
		
	}
//*************************************************************************************// 
	public function remove(Request $request,$id){
		$page = Page::find($id);
		$page->delete();
		$request->session()->flash('status', 'Page Deleted Successfully');
		return redirect('admin/pages/list');
	}
//*************************************************************************************// 
	public function page($slug){
		$page = Page::where('slug', $slug)->first();
		//echo "<pre>"; print_r($page); die();
		return view('page',compact('page'));
	}
//*************************************************************************************// 
	public function imageUpload(Request $request){
		$image = $request->file('file');
		echo $filename  = time() . '.' . $image->getClientOriginalExtension();
		$path = public_path('images/gallery/' . $filename);
		Image::make($image->getRealPath())->save($path);
	}
//*************************************************************************************// 
	public function addTiles(Request $request){
		if ($request->isMethod('post')) {
			$requestTile = $request->all();
			if(!isset($requestTile['insert_code'])){
				$requestTile['all_codes'] = true;
				$requestTile['insert_code'] = '';
			}else{
				$requestTile['all_codes'] = false;
			}
			if($requestTile['type'] == 'image'){
				if($request->file('image')){
					$image = $request->file('image');
					$filename  = time() . '.' . $image->getClientOriginalExtension();
					$path = public_path('images/tiles/normal/'.$filename);
					$path200 = public_path('images/tiles/200by200/'.$filename);
					$img = Image::make($image->getRealPath())->save($path);
					$img = Image::make($image->getRealPath());
					$img->resize(200, 200, function ($constraint) {})->save($path200);
					$requestTile['image'] = $filename;
				}
				

				$tile = Tile::create($requestTile);
				$request->session()->flash('status', 'Picture Tile Added Successfully');
				return redirect('admin/tiles');
			}else{
				$tile = Tile::create($requestTile);
				$request->session()->flash('status', 'Picture Tile Added Successfully');
				return redirect('admin/tiles');
			}
			
		}else{
			
			return view('admin/pages/addtiles');
		}
	}
//*************************************************************************************// 
	function tiles(){
		$tiles = Tile::all();
		return view('admin/pages/tiles',compact('tiles'));
	}
//*************************************************************************************// 
	public function editTile(Request $request,$id){
		$tile = Tile::find($id);
		if ($request->isMethod('put')) {
			$requestTile = $request->except(['_method', '_token']);
			if(!isset($requestTile['insert_code'])){
				$requestTile['all_codes'] = true;
				$requestTile['insert_code'] = '';
			}else{
				$requestTile['all_codes'] = false;
			}
			if($requestTile['type'] == 'image'){
				if($request->file('image')){
					$image = $request->file('image');
					$filename  = time() . '.' . $image->getClientOriginalExtension();
					$path = public_path('images/tiles/normal/'.$filename);
					$path200 = public_path('images/tiles/200by200/'.$filename);
					$img = Image::make($image->getRealPath())->save($path);
					$img = Image::make($image->getRealPath());
					$img->resize(200, 200, function ($constraint) {})->save($path200);
					$requestTile['image'] = $filename;
				}else{
					$requestTile['image'] = $tile['image'];
				}
				
				$tile = Tile::where('id', $id)
				->update($requestTile);

				$request->session()->flash('status', 'Picture Tile Updated Successfully');
				return redirect('admin/tiles');
			}else{
				$tile = Tile::where('id', $id)
				->update($requestTile);
				$request->session()->flash('status', 'Picture Tile Updated Successfully');
				return redirect('admin/tiles');
			}

		}else{

			return view('admin/pages/editTile',compact('tile'));
		}
	}
//*************************************************************************************// 
	public function removeTile(Request $request,$id){
		$tile = Tile::find($id);
		if($tile['type'] == 'image'){
			unlink(public_path().'/images/tiles/normal/'.$tile['image']);
			unlink(public_path().'/images/tiles/200by200/'.$tile['image']);
		}
		$tile->delete();
		$request->session()->flash('status', 'Tile Deleted Successfully');
		return redirect('admin/tiles');
	}
//*************************************************************************************// 	
}