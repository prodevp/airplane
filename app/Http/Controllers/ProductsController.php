<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Peron\AmazonMws\AmazonInventoryList;
use Peron\AmazonMws\AmazonProductList;
use App\Product;
use App\InsertCode;
use App\PromoCode;
use App\BigDiscountCode;
use Illuminate\Support\Facades\Storage;
use Validator;
use Image;
use App\Http\Controllers\AmazonAPIController;

class ProductsController extends Controller
{
//*************************************************************************************//     
   public function getProducts(){
        $keyId      = 'AKIAJD2BCWU2ZCQB6JHA';
        $secretKey  = '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';
        $associateId    = 'app0b8-20';

        $amazonAPI = new AmazonAPIController( $keyId, $secretKey, $associateId );
        $amazonAPI->SetLocale( 'us' );
        $amazonAPI->SetSSL( true );
        $items = $amazonAPI->ItemLookUp('B00R55AEUO');
        //echo '<pre>';print_r($items);die;
        $inventries = $this->fetchInventry();
        try {
            foreach ($inventries as $inventry) {
                $asin[] = $inventry['ASIN'];
            }
            $pro = array_chunk($asin, 4);
            //echo '<pre>';print_r($pro);die;
           // foreach($pro as $p){
            for($i=0;$i<count($pro);$i++){
                $products = $this->fetchProducts($pro[$i]);
                if(is_array($products)){
                 foreach ($products as $product) {
                    $product = $product->getData();
                    $amazonProduct = new Product;
                    $amazonProduct->name = $product['AttributeSets'][0]['Title'];
                    if(isset($product['AttributeSets'][0]['Brand'])){
                        $amazonProduct->Brand = $product['AttributeSets'][0]['Brand'];
                    }
                    if(isset($product['AttributeSets'][0]['Feature'])){
                        $amazonProduct->Feature = json_encode($product['AttributeSets'][0]['Feature']);
                    }
                    if(isset($product['AttributeSets'][0]['ListPrice'])){
                        $amazonProduct->price = $product['AttributeSets'][0]['ListPrice']['Amount'];
                    }
                    $amazonProduct->amazon = true;
                    $amazonProduct->image = $product['AttributeSets'][0]['SmallImage']['URL'];
                    if(isset($product['AttributeSets'][0]['Color'])){
                        $amazonProduct->Color = $product['AttributeSets'][0]['Color'];
                    }
                    $amazonProduct->save();

                }

            }

        }
        die('Inserted');
    }catch (Exception $ex) {
        echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}
//*************************************************************************************// 
    public function fetchInventry(){
     try {
            $obj = new AmazonInventoryList("Slope Bags"); //store name matches the array key in the config file
            $obj->setUseToken(); //tells the object to automatically use tokens right away
            $obj->setStartTime("- 24 hours");
            $obj->fetchInventoryList(); //this is what actually sends the request
            return $obj->getSupply();//tells the object to automatically use tokens right away

        } catch (Exception $ex) {
            echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
        }

    }
//*************************************************************************************// 
    public function fetchProducts($asin){
     try {
       // echo '<pre>';print_r($asin);die;
            $obj = new AmazonProductList("Slope Bags"); //store name matches the array key in the config file
            $obj->setIdType('ASIN'); //tells the object to automatically use tokens right away
            $obj->setProductIds($asin);
            return $obj->fetchProductList(); //this is what actually sends the request

        } catch (Exception $ex) {
            echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
        }
    }
//*************************************************************************************// 
    public function add(Request $request)
    {
        if ($request->isMethod('post')) {

            $product_data = $request->all();
             
            $validator = Validator::make($product_data, [
                'name' => 'required',
                'image'  => 'required',
                'sku'  => 'required',
                'ecode'  => 'required',
                'pcode'  => 'required',
                ])->validate();

            if($request->file('image')){
                $image = $request->file('image');            
                $filesname  = time() . '.' . $image->getClientOriginalExtension();

                $path = public_path('images/products/normal/'.$filesname);
                Image::make($image->getRealPath())->save($path);
            
             if ($request->file('ecode')) {
                $extension = $request->ecode->extension();
                $product['name'] = $product_data['name'];
                $product['sku'] = $product_data['sku'];
                $product['description'] = $product_data['description'];
                $product['image'] = $filesname;
                // echo "<pre>";print_r($product);die();
                $insrt_code_filename =   Storage::disk('ecode')->put('', $request->file('ecode'));
                $product['filename'] = $insrt_code_filename ;
                $products =  Product::create($product);
                //echo "<pre>"; print_r($insrt_code_filename);die();            
                $codes = array();
                $filename = public_path('ecode/'.$insrt_code_filename) ; // Code Text File
               // echo "<pre>"; print_r($filename);die();
                if (file_exists($filename) && is_readable ($filename)) {
                  $fileResource  = fopen($filename, "r");
                  if ($fileResource) {
                  while (($line = fgets($fileResource)) !== false) {
                  $codes[] = trim(preg_replace('/\s\s+/', ' ', $line));
                 }
                   fclose($fileResource);
                  }
                }
                for($i=0;$i<count($codes);$i++){
                    $insertcode =  new InsertCode();
                    $insertcode->product_id = $products['id'];
                    $insertcode->status = true ;
                    $insertcode->insert_code = $codes[$i];
                    $insertcode->save();
                  }
                }
             if ($request->file('pcode')) {
                $extension = $request->ecode->extension();
                $insrt_code_filename =   Storage::disk('pcode')->put('', $request->file('pcode'));
                $pcode = array();
                $filename = public_path('pcode/'.$insrt_code_filename) ; // Code Text File
                // echo "<pre>"; print_r($filename);die();
                if (file_exists($filename) && is_readable ($filename)) {
                $fileResource  = fopen($filename, "r");
                if ($fileResource) {
                while (($line = fgets($fileResource)) !== false) {
                $pcode[] = trim(preg_replace('/\s\s+/', ' ', $line));
                  }
                    fclose($fileResource);
                  }
                }
                for($i=0;$i<count($pcode);$i++){
                    $insertcode =  new PromoCode();
                    $insertcode->product_id = $products['id'];
                    $insertcode->status = true ;
                    $insertcode->promo_code = $pcode[$i];
                    $insertcode->save(); 
                }

           
               
                $request->session()->flash('status', 'Product Added Successfully'); 
                return redirect('admin/products');
               
              }
            }
        }else{
            return view('admin/product/add');
        }
    }
//*************************************************************************************// 
    public function productlist(){
        $products = Product::all();
        return view('admin/product/list',compact('products'));
    }
//*************************************************************************************// 
    public function edit(Request $request,$id){
            $product = Product::find($id);
            //echo "<pre>"; print_r($product); die();
            if ($request->isMethod('PUT')) {
                $requestProduct = $request->except(['_method', '_token']);
                $libraryEdit = array();
                Validator::make($requestProduct, [
                    'name'  => 'required',
                    'insert_code'  => 'required'
                    ])->validate();

               
               if($request->file('image')){
                $image = $request->file('image');
                $filename  = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('images/products/normal/'.$filename);
                Image::make($image->getRealPath())->save($path);
                  
              
                $product->name = $requestProduct['name'];
                $product->description = $requestProduct['description'];
                $product->insert_code = $requestProduct['insert_code'];
                $requestProduct['image'] = $filename;
                }
                $product = Product::where('id', $id)
                ->update($requestProduct);


                $request->session()->flash('status', 'Product Edited Successfully');
                return redirect('admin/products');
            
        }else{       
             return view('admin/product/edit',compact('product'));
           }
        }
//*************************************************************************************// 
    public function removeProduct(Request $request,$id){
        $product = Product::find($id);
        // $product = InsertCode::where('product_id')->delete();
        // $product = PromoCode::where('product_id')->delete();
        if($product['type'] == 'image'){
         unlink(public_path().'/images/products/normal/'.$product['image']);
     }
     $product->delete();
     $request->session()->flash('status', 'Product Deleted Successfully');
     return redirect('admin/products');
    }
//**************************************************************************************//
     public function addcode(Request $request){
       if($request->hasFile('ecode')){
                $path = $request->file('ecode')->getRealPath();

                $data = Product::load($path, function($reader) {})->get();

                if(!empty($data) && $data->count()){

                    foreach ($data->toArray() as $key => $value) {
                        if(!empty($value)){
                            foreach ($value as $v) {        
                                $insert[] = ['insert_code' => $v['insert_code']];
                            }
                        }
                    }

                    if(!empty($insert)){
                        InsertCode::insert($insert);
                        return back()->with('success','Insert Record successfully.');
                    }
                }
            }

            return back()->with('error','Please Check your file, Something is wrong there.');
        }
           

//*************************************************************************************//
      
  public function fetchinsert_codes( $id){
     
        //$insertcodes = InsertCode::find($id);
       $products_id = InsertCode::where('product_id','=', $id)->get();
     // echo "<pre>"; print_r($products_id); die();
       return view('admin/product/codelist', compact('products_id'));
       
    }
//*************************************************************************************//
   public function fetchpromo_code($id){
     //echo "<pre>";print_r($id);die();
     $promo_code_id = PromoCode::where('product_id','=', $id)->get();
     return view('admin/product/promocode', compact('promo_code_id'));
   } 
//*************************************************************************************// 
   public function big_discount_code(Request $request)
     {
         if ($request->isMethod('post')) {
            $big_discount = $request->all();
             $validator = Validator::make($big_discount, [
                'big_code' => 'required',
              
                ])->validate();
        if ($request->file('big_code')) {
           $extension = $request->ecode->extension();
           $insrt_code_filename =   Storage::disk('big_code')->put('', $request->file('big_code'));
           $big_code = array();
           $filename = public_path('big_code/'.$insrt_code_filename) ; // Code Text File
           // echo "<pre>"; print_r($filename);die();
           if (file_exists($filename) && is_readable ($filename)) {
           $fileResource  = fopen($filename, "r");
           if ($fileResource) {
           while (($line = fgets($fileResource)) !== false) {
           $big_code[] = trim(preg_replace('/\s\s+/', ' ', $line));
             }
               fclose($fileResource);
             }
           }
           for($i=0;$i<count($big_code);$i++){
               $insertcode =  new BigDiscountCode();
               $insertcode->product_id = $products['id'];
               $insertcode->promo_code = $big_code[$i];
               $insertcode->save(); 
           }
           $request->session()->flash('status', 'Product Added Successfully'); 
                return redirect('admin/products');
            }
            }
      }
//*************************************************************************************//

}
