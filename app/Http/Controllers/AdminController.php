<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use File;
use Validator;
use App\Facility;
use App\Vendor;

class AdminController extends Controller
{
//*************************************************************************************//     
    protected function editprofile(){
    	return view('admin/editprofile');
    }
//*************************************************************************************// 
    protected function login(Request $request){

    	$requestlogin = $request->all();
    	
    	if($requestlogin){
    		if (Auth::attempt(['email' => $requestlogin['email'], 'password' => $requestlogin['password']])) {
            return redirect('admin/dashboard');
        	}
    	}else{
           return view('admin/login');
        }
    	
    }
//*************************************************************************************// 
    public function forgot(){
    	return view('admin/forgot');
    }
//*********facilities****************************************************************************//  
    public function facility(){
        $facility = Facility::orderBy('created_at', 'desc')->get();

         //$facility = Facility::all();
            return view('admin/facility/list',compact('facility'));
        }
    //*************************************************************************************// 
         public function addfacility(Request $request){
         if ($request->isMethod('post')) {
            $product_data = $request->all();
        
                 $validator = Validator::make($product_data, [
                'company_name'  => 'required',
                'email'  => 'required|email',
               'telephone'  => 'required',
               'address'  => 'required',
                'site'  => 'required|url',
                 'type'  => 'required',
                 'country'=>'required',
                 'state'=>'required',
                 'city'=>'required'
                 ])->validate();
            
        $insertcode = DB::table('facilities')->insert(
    ['company_name' => $product_data['company_name'], 'telephone' => $product_data['telephone'],'email' => $product_data['email'],'address'=>$product_data['address'],'test_cell'=>$product_data['test_cell'],'site'=>$product_data['site'],'type'=>$product_data['type'],'checks'=>$product_data['checks'],'country'=>$product_data['country'],'state'=>$product_data['state'],'city'=>$product_data['city'],'postbox'=>$product_data['postbox']]  
      );  
           if($insertcode){
               
                $request->session()->flash('status', 'Facility Added Successfully'); 
                return redirect('admin/facility');
              } 
        }else{
            return view('admin/facility/add');
       }
    }

    //*************************************************************************************//   

    
     public function editfacility(Request $request,$id){
     
            $facility = Facility::find($id);
            //echo "<pre>"; print_r($product); die();
            if ($request->isMethod('PUT')) {
                $requestProduct = $request->except(['_method', '_token']);
                $libraryEdit = array();

                  $validator = Validator::make($requestProduct, [
                'company_name'  => 'required',
                'email'  => 'required|email',
               'telephone'  => 'required',
               'address'  => 'required',
                'site'  => 'required|url',
                 'type'  => 'required',
                  'country'=>'required',
                 'state'=>'required',
                 'city'=>'required'
                 ])->validate();
               
         
                $product = Facility::where('id', $id)
                ->update($requestProduct);

                $request->session()->flash('status', 'Facility Edited Successfully');
                return redirect('admin/facility');
            
        }else{       
             return view('admin/facility/edit',compact('facility'));
           }
        }
/************remove facility**********/

         public function removefacility(Request $request,$id){
            $facility = Facility::find($id);
          
        $facility->delete();
        $request->session()->flash('status', 'Facility Deleted Successfully');
            return redirect('admin/facility');
          }

          //*********MRO Vendor****************************************************************************//  
    public function vendor(){
          $vendor = Vendor::orderBy('created_at', 'desc')->get();

        // $vendor = Vendor::all();
            return view('admin/vendor/list',compact('vendor'));
        }
    //*************************************************************************************// 
         public function addvendor(Request $request){
         if ($request->isMethod('post')) {
            $product_data = $request->all();
        
                 $validator = Validator::make($product_data, [
                    'name'  => 'required',
                'company_name'  => 'required',
                'email'  => 'required|email',
               'telephone'  => 'required',
               'address'  => 'required',
               'country'  => 'required',
                'state'  => 'required',
                'site'  => 'required|url',
                'city'  => 'required',
                 'facility'  => 'required',
                 ])->validate();
            
        $insertcode = DB::table('vendors')->insert(
    ['company_name' => $product_data['company_name'], 'telephone' => $product_data['telephone'],'email' => $product_data['email'],'address'=>$product_data['address'],'country'=>$product_data['country'],'site'=>$product_data['site'],'state'=>$product_data['state'],'name'=>$product_data['name'],'facility'=>$product_data['facility'],'city'=>$product_data['city']]  
      );  
           if($insertcode){
               
                $request->session()->flash('status', 'Vendor Added Successfully'); 
                return redirect('admin/vendor');
              } 
        }else{
            return view('admin/vendor/add');
       }
    }

    //*************************************************************************************//   

    
     public function editvendor(Request $request,$id){
     
            $vendor = Vendor::find($id);
            //echo "<pre>"; print_r($product); die();
            if ($request->isMethod('PUT')) {
                $requestProduct = $request->except(['_method', '_token']);
                $libraryEdit = array();

                 $validator = Validator::make($requestProduct, [
                    'name'  => 'required',
                'company_name'  => 'required',
                'email'  => 'required|email',
               'telephone'  => 'required',
               'address'  => 'required',
               'country'  => 'required',
                'state'  => 'required',
                'site'  => 'required|url',
                'city' =>'required',
                 'facility'  => 'required',
                 ])->validate();
               
         
                $product = Vendor::where('id', $id)
                ->update($requestProduct);

                $request->session()->flash('status', 'Vendor Edited Successfully');
                return redirect('admin/vendor');
            
        }else{       
             return view('admin/vendor/edit',compact('vendor'));
           }
        }
/************remove facility**********/

         public function removeVendor(Request $request,$id){
            $facility = Vendor::find($id);
        $facility->delete();
        $request->session()->flash('status', 'Vendor Deleted Successfully');
            return redirect('admin/vendor');
          }

        }
