<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\Library;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dashboard['members'] =   User::where('role','!=', 'admin')->count();
        $dashboard['customers'] = Customer::count();
        $dashboard['libraries'] = Library::count();
        $dashboard['products'] = Product::count();
        return view('home',compact('dashboard'));
    }
}