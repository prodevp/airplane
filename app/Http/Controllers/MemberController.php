<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StaffRequest;
use App\User;
use App\StaffInfo; 
use DB;
use Image; 
use Carbon\Carbon;
use Validator;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Peron\AmazonMws\AmazonOrderList; 
use Peron\AmazonMws\AmazonOrderItemList; 
use App\Customer;
use App\InsertCode;
use App\Product;
use App\PromoCode;
use App\BigDiscountCode;
use Illuminate\Support\Facades\Storage;
use App\CustomerOrder;
use File;
use Newsletter;
use View;
use Barryvdh\DomPDF\Facade as PDF;
use NZTim\Mailchimp\Mailchimp;

class MemberController extends Controller
{
//***************************************************************************//
  protected function getSavedImageName( $timestamp, $image )
  {
    return $timestamp . '-' . $image->getClientOriginalName();
  }
//***************************************************************************//
  protected function getFormattedTimestamp()
  {
    return str_replace( [' ', ':'], '-', Carbon::now()->toDateTimeString() );
  }
//***************************************************************************//
  public function checkValidation($request){


   return Validator::make($request, [

     'StaffInfo.home_no' => 'numeric',
     'User.phone' => 'numeric',
     'StaffInfo.home_no' => 'numeric',
     'StaffInfo.work_no' => 'numeric',
     'StaffInfo.work_ext' => 'required',
     'StaffInfo.fax_no' => 'numeric',
     'StaffInfo.other_no' => 'numeric',
     'StaffInfo.mobile_provider' => 'required',
     'StaffInfo.type' => 'required',
     'StaffInfo.address' => 'required',
     'StaffInfo.state' => 'required',
     'StaffInfo.country' => 'required',
     'User.email' => 'required|email|unique',
     'User.name' => 'required|unique'

     ]); 
 }
//***************************************************************************//
 protected function admin_addMember(Request $request){
  if ($request->isMethod('post')) {
   $requeststaff = $request->all();
   $validator = Validator::make($requeststaff, [
    'email' => 'unique:users,email|required',
    'name'  => 'required',
    'phone'  => 'numeric'
    ])->validate();
   $staff = new User;
   $staff->name = $request['name'];
   $staff->password = bcrypt($request['password']);
   $staff->email = $request['email'];
   $staff->phone = $request['phone'];
   $staff->role = 'member';
        $staff->save(); // save to users table
        $request->session()->flash('status', 'Users Added Successfully'); 
        return redirect('admin/user');
      }else{
        return view('admin/staff/addmember');
      }

    }
//***************************************************************************//
    public function admin_editMember(Request $request,$id){

      if ($request->isMethod('PUT')) {
        $requestmember = $request->except(['_method', '_token']); 
        Validator::make($requestmember, [
          'name'  => 'required',
          'phone'  => 'numeric'
          ])->validate();
        User::where('id', $id)
        ->update($requestmember);
        $request->session()->flash('status', 'Users Edited Successfully');
         return redirect('admin/user');
      }else{
       $member = User::find($id);
       return view('admin/staff/editmember',compact('member'));
     }

   }
//***************************************************************************//
   public function remove(Request $request,$id){
    $member = User::find($id);
    $member->delete();
    $request->session()->flash('status', 'Users Deleted Successfully');
    return redirect('admin/user');
  }
//***************************************************************************//
  protected function admin_staffs(){
             // $vendor = Vendor::orderBy('created_at', 'desc')->get();
    $staffs = User::where('role', 'member')->orderBy('created_at', 'desc')->get();
    return view('admin/staff/staffs',['staffs'=>$staffs]);
  }
//***************************************************************************//
  public function admin_staffprofile($id){

   $staffsprofile = User::with('staffdata')->find($id);
   return view('admin.staff.staff profile',compact('staffsprofile'));

 }

//***************************************************************************//
 function getAmazonOrders(Request $request) {

  $orders = $this->getOrders();

   //echo "<pre>"; print_r($orders); die();
  foreach ($orders as $order) {
        //these are AmazonOrder objects
    $customer['name'] = $order->getBuyerName();
    $customer['email'] = $order->getBuyerEmail();
    $address = $order->getShippingAddress();
    $customer['address_name'] = $address['Name'];
    $customer['address_line'] = $address['AddressLine1'].' '.$address['AddressLine2'].' '.$address['AddressLine3'];
    $customer['city'] = $address['City'];
    $customer['postal_code'] = $address['PostalCode'];
    $customer['country_code'] = $address['CountryCode'];
    $customer['phone'] = $address['Phone'];
    $customer['order_number'] = $order->getAmazonOrderId();
    $newCustomer = Customer::create($customer);
    /*echo $newCustomer['id'] = // customer id
    $items = $this->getOrderItems($order->getAmazonOrderId);
    $iter, customer_id*/
    $items = $this->getOrderItems($order->getAmazonOrderId());
    foreach ($items as $key => $item) {
          $customer_orders['customer_id'] = $newCustomer['id'] ; // customer id
          $customer_orders['asin'] = $item['ASIN'];
          $customer_orders['title'] = $item['Title'];
          
          if(isset($item['PromotionIds']))  {
            $promotion_ids = $item['PromotionIds'];
            $customer_orders['promotion_id'] = json_encode($promotion_ids);  
          }
          $newCustomerOrder = CustomerOrder::create($customer_orders);
        }

      }
  $request->session()->flash('status', 'Customer Imported Successfully');

}
//***************************************************************************//
function getOrders(){
  try {
    $orders = new AmazonOrderList("Slope Bags");
         //store name matches the array key in the config file
        $orders->setLimits('Created', "- 3 years"); //accepts either specific timestamps or relative times 
        $orders->setFulfillmentChannelFilter("MFN"); //no Amazon-fulfilled orders
        $orders->setOrderStatusFilter(
          array("Shipped")
            ); ///no shipped or pending orders
        $orders->setUseToken(); //tells the object to automatically use tokens right away
        $orders->fetchOrders(); //this is what actually sends the request
        $all_orders = $orders->getList();
        return $all_orders;        
      } catch (Exception $ex) {
        echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
      }
    }
//***************************************************************************//
    function fetchCustomers(){
      $customers = Customer::where('name','!=','')->orderBy('id', 'desc')->get();
      return view('admin/customer/list',compact('customers'));
    }
//***************************************************************************//
    function insertCodes(){
      $codes = array();
      $filename = public_path('codes/A15983MAE5B60G-8291391942823047333.txt'); // Code Text File
      if (file_exists($filename) && is_readable ($filename)) {
        $fileResource  = fopen($filename, "r");
        if ($fileResource) {
          while (($line = fgets($fileResource)) !== false) {
            $codes[] = trim(preg_replace('/\s\s+/', ' ', $line));
          }
          fclose($fileResource);
        }
      }
      $customers = Customer::where('name','!=','')->orderBy('id', 'desc')->take(count($codes))->get();
      foreach ($customers as $key => $customer) {
        Customer::where('id', $customer->id)
        ->update(['insert_code' => $codes[$key]]); // Update Code of each customer
      }
      echo 'Code Inserted Successfully';
      
    }
//***************************************************************************//
    public function signup(Request $request){
       if ($request->isMethod('post')) {
       $requestUser = $request->all();
       $validator = Validator::make($requestUser,[
        'email'       => 'required',
        'insert_code' => 'required'
        ])->validate();

       $email = $requestUser['email'];
       // echo "<pre>";print_r($email);die();
       $subscribes = Newsletter::subscribe($email, ['FNAME' => 'First name', 'LNAME' => 'Last name'], 'subscribers');
       $insert_code = $requestUser['insert_code'];
       $request->session()->put('insert_code',$insert_code);
       $findinsert_code    = InsertCode::where('insert_code',$insert_code)->first();
       $findid      = $findinsert_code->product_id;
       $promocode   = PromoCode::where('product_id',$findid)->inRandomOrder()->limit(1)->first();
       $findpromo_code = $promocode->promo_code;
       $request->session()->put('promo_code',$findpromo_code);
       $findUser = InsertCode::where('insert_code',$insert_code)->first();
       //echo "<pre>";print_r($findpromo_code);die();
       if(!$findUser){
         $request->session()->flash('status', 'Invalid Insert Code!');
         return redirect('signup'); 
       } 
       else{
        $user = new User();
         if($requestUser['insert_code']){
           $user->insert_code = $requestUser['insert_code'];
         }else{
           $user->insert_code = $findUser->insert_code;
         }
         $user->password = bcrypt('secret');
         $user->email = $requestUser['email'];
         $user->insert_code = $requestUser['insert_code'];
         $user->promo_code =  $findpromo_code;
         $user->role = 'member';
         $user->save();
         //$request->session()->flash('status', 'Sign Up Successfully');
        return redirect('/frontpage');
       }
    }else{
     return view('front/signup');
   }

 }
//***************************************************************************//
     public function signupcode(Request $request){

      if ($request->isMethod('post')) {

        $requestUser = $request->all();

        $validator = Validator::make($requestUser, [
         'email' => 'unique:users,email|required',
         'order_number'  => 'required',
         'name'  => 'required'
         ])->validate();

        $ordernumber = $requestUser['order_number'];
        $findUser  = Customer::where('order_number', 'LIKE', '%'.$ordernumber)->first(); 
        $bigdiscountcode   = BigDiscountCode::inRandomOrder()->limit(1)->first();
        $find_bigdiscountcode = $bigdiscountcode->promo_code;
           // echo "<pre>";print_r($find_bigdiscountcode);die();
        $request->session()->put('promo_code',$find_bigdiscountcode);

        if(!$findUser){
         $request->session()->flash('status', 'Invalid Post Code Number!');
         return redirect('postalcode');
        }
         else{
           $order_number = $findUser->order_number;
           $address_name = $findUser->address_name;
           $address_line = $findUser->address_line;
           $city = $findUser->city;
           $postal_code = $findUser->postal_code;
           $request->session()->put('order_number',$order_number);
           $user = new User();
           if($requestUser['name']){
           $user->name = $requestUser['name'];
           }else{
           $user->name = $order_number->name;
           }
           $user->password = bcrypt('secret');
           $user->email = $requestUser['email'];
           $user->order_number = $order_number;
           $user->address_name = $address_name;
           $user->address_line = $address_line;
           $user->city = $city;
           $user->promo_code =  $find_bigdiscountcode;
           $user->postal_code = $postal_code;
           $user->role = 'member';
           $user->save();
           $request->session()->flash('status', 'Sign Up Successfully');
           return redirect('/frontpages');
       }
     }
     else{
      return view('front/postalcode');
    }

  }
//***************************************************************************//
 public function getOrderItems($AmazonOrderId){
  
   try {
        $order_items = new AmazonOrderItemList("Slope Bags");
        $order_items->setUseToken();
        // $order_items->setOrderId($AmazonOrderId); 
        $order_items->setOrderId($AmazonOrderId); 
        $order_items->fetchItems();
        return $order_items->getItems();
        //$result = $order_items->getItems();
        //echo "<pre>"; print_r($result); die();
      } catch (Exception $ex) {
        echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
      }
 }
//***************************************************************************//
 
 public function editcode(Request $request){
    if ($request->isMethod('POST')) {
       $requestcustomer = $request->except(['_method', '_token']); 
       $id = $request['id'];
       $updatecustomer = Customer::where('id', $id)
        ->update($requestcustomer);
       if($updatecustomer) {
          return "success";
       }
          else{
            return "error occured";    
          }
      }
  }
//***************************************************************************//
      
  public function staffdetail( $id){
       //  $insertcodes = User::find($id);
       $staff_detail = User::where('id','=', $id)->get();
       // echo "<pre>"; print_r($insertcodes); die();
      return view('admin/staff/staffdetail',compact('staff_detail'));
    }


//***************************************************************************// 
  public function contactdetail(){
       
       $contact_detail = Message::all();
      return view('admin/customer/contact',compact('contact_detail'));
    }
//***************************************************************************//  
  function all_customer_detail(){
      $customers_detail = Customer::where('name','!=','')->orderBy('id', 'desc')->get();
      $html = '';
      $html = "<div>";
     foreach ($customers_detail as $key => $customer) {
       $html .= "<div>";
       $html .= 'Name : '.$customer->name;
       $html .= "</div>";
       $html .= "<div>";
       $html .= 'Order Number : '.$customer->order_number;
       $html .= "</div>";
       $html .= "<div>";
       $html .= 'Address : '.$customer->address_name .$customer->address_line ;
       $html .= "</div>";
       $html .= "<div>";
       $html .= 'City : '.$customer->city;
       $html .= "</div>";
       $html .= "<div>";
       $html .= 'Postal Code : '.$customer->postal_code;
       $html .= "</div><br /><hr>";
     }
       set_time_limit(800);
       $pdf = PDF::loadHTML($html);
       // $pdf = PDF::loadView($file);
       return $pdf->download('customers_detail.pdf'); //this code is used for the name pdf
       // return $pdf->stream('invoice.pdf');
        
    }

//***************************************************************************//
     public function download(Request $request)
    {
        $customers_detail =Customer::where('name','!=','')->orderBy('id', 'desc')->get();;
        view()->share('customers_detail',$customers_detail);
        $pdf = PDF::loadView('admin/customer/detail', $customers_detail);
        return $pdf->stream('invoice.pdf');
        // return $pdf->download('detail.pdf');
    }
//***************************************************************************// 
  //   public function mailchimp(){
  //     return view('admin/customer/detail');
  // } 
//***************************************************************************//
public function mailchimp(Request $request){
      if ($request->isMethod('post')) {
            $requestUser = $request->all();
            $email  = $requestUser['email'];
                  
            $subscribes = Newsletter::subscribe($email, [], 'subscribers');
            return $subscribes;
            die('Done');
        }
        else{
         return view('admin.customer.detail');
       }
     }
//***************************************************************************//                                                                                                                                                                                                                                 
       
}
