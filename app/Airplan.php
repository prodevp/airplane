<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airplan extends Model
{
    //
     protected $fillable = [
         'airType', 'airVariant','image','variable','MSN','ESN','utilisation','cycle','engineType','timeTaken','category','TT','TC','facility','manufacture','engin_TC','engin_TT','TSLSV','CSLSV','reason','bought','price','userId'
    ];
	
}
