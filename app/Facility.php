<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    //
     protected $fillable = [
        'company_name', 'email','telephone','address','test_cell','checks','site','type','fax','country','state','city','postbox'
    ];
	
}