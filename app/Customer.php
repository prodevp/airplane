<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
	protected $fillable = ['name','email','address_name','address_line','city','postal_code','country_code','order_number','phone'];
}
